import mongoose from "mongoose"

const trialSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    gym: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Gym",
      required: true,
    },
    startDate: {
      type: String,
    },
    endDate: {
      type: String,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: false,
    },
    isClaimed: {
      type: Boolean,
      required: true,
      default: false,
    },
  },
  {
    timestamps: true,
  }
)

const Trial = mongoose.model("Trial", trialSchema)
export default Trial
