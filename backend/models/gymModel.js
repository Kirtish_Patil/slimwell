import mongoose from "mongoose"

const reviewSchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
})

const gymSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  location: {
    type: String,
    required: true,
  },
  address: {
    type: String,
    required: true,
  },
  cell: {
    type: String,
    required: true,
    trim: true,
    unique: true,
  },
  email: {
    type: String,
    required: true,
  },
  ratings: {
    type: Number,
    required: true,
    default: 0,
  },
  numReviews: {
    type: Number,
    required: true,
    default: 0,
  },
  imgs: [
    {
      title: {
        type: String,
      },
      contentType: {
        type: String,
      },
      imgPath: {
        type: String,
      },
    },
  ],
  reviews: [reviewSchema],
})

const Gym = mongoose.model("Gym", gymSchema)
export default Gym
