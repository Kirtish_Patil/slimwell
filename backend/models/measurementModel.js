import mongoose from "mongoose"

const measurementSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    weight: {
      type: Number,
      required: true,
    },
    neckCircumference: {
      type: Number,
      required: true,
    },
    chestRelaxed: {
      type: Number,
      required: true,
    },
    chestExpanded: {
      type: Number,
      required: true,
    },
    waistRelaxed: {
      type: Number,
      required: true,
    },
    waistExpanded: {
      type: Number,
      required: true,
    },
    bicepsRelaxed: {
      type: Number,
      required: true,
    },
    bicepsExpanded: {
      type: Number,
      required: true,
    },
    forearms: {
      type: Number,
      required: true,
    },
    thighs: {
      type: Number,
      required: true,
    },
    calfs: {
      type: Number,
      required: true,
    },
    wrist: {
      type: Number,
      required: true,
    },
  },
  { timestamps: true }
)

const Measurement = mongoose.model("Measurement", measurementSchema)

export default Measurement
