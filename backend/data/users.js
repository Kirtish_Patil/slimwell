import bcrypt from "bcryptjs"

const users = [
  {
    name: "SuperAdmin",
    email: "super@example.com",
    password: bcrypt.hashSync("123456", 10),
    role: "superAdmin",
    position: "owner",
    verificationKey: "slimwell_superAdmin",
  },
  {
    name: "Admin",
    email: "admin@example.com",
    password: bcrypt.hashSync("123456", 10),
    position: "manager",
    role: "admin",
    verificationKey: "slimwell_admin",
  },
  {
    name: "John",
    email: "john@example.com",
    password: bcrypt.hashSync("123456", 10),
    role: "user",
  },
  {
    name: "Jane",
    email: "jane@example.com",
    password: bcrypt.hashSync("123456", 10),
    role: "user",
  },
  {
    name: "Jaden",
    email: "jaden@example.com",
    password: bcrypt.hashSync("123456", 10),
    role: "user",
  },
  {
    name: "Jasmine",
    email: "jasmine@example.com",
    password: bcrypt.hashSync("123456", 10),
    role: "user",
  },
]

export default users
