const gyms = [
  {
    name: "Slimwell Fitness Club",
    location:
      "https://www.google.com/maps/place/Slimwell+Fitness+Club/@19.0352341,72.8411296,15z/data=!4m6!3m5!1s0x3be7c92d07d0186d:0x9cd4a8cb40f8158e!8m2!3d19.0352341!4d72.8411296!16s%2Fg%2F1tcy2r_r?entry=ttu",
    address:
      "Hm Gym, Opposite Saraswat Colony, Ram Mohadikar Marg, off Shitladevi, Temple road, Behind Victoria Church, Mahim, Mumbai, Maharashtra 400016",
    cell: "09324798829",
    email: "slimwell@example.com",
    ratings: 4.5,
    numReviews: 10,
    imgs: [
      {
        title: "Slimwell Fitness Club",
        contentType: "/images/jpg",
        imgPath: "/SlimwellFitnessClub/SlimWell_Mahim_stock1.jpg",
      },
      {
        title: "Slimwell Fitness Club",
        contentType: "/images/jpg",
        imgPath: "/SlimwellFitnessClub/SlimWell_Mahim_stock2.jpg",
      },
      {
        title: "Slimwell Fitness Club",
        contentType: "/images/jpg",
        imgPath: "/SlimwellFitnessClub/SlimWell_Mahim_stock3.jpg",
      },
      {
        title: "Slimwell Fitness Club",
        contentType: "/images/jpg",
        imgPath: "/SlimwellFitnessClub/SlimWell_Mahim_stock4.jpg",
      },
      {
        title: "Slimwell Fitness Club",
        contentType: "/images/jpg",
        imgPath: "/SlimwellFitnessClub/SlimWell_Mahim_stock5.jpg",
      },
    ],
    numReviews: 12,
  },
  {
    name: "Slimwell Dadar",
    location:
      "https://www.google.com/maps/place/Slimwell+Dadar/@19.0281942,72.8345975,17z/data=!3m1!4b1!4m6!3m5!1s0x3be7cece618c7f61:0x7ed32e7544a48efa!8m2!3d19.0281891!4d72.8371724!16s%2Fg%2F11c31rr0yz?entry=ttu",
    address:
      "Modak Bunglow, Swatantryaveer Savarkar Vyayamshala, Swatantryaveer Savarkar Rashtriya Smarak Veer Savarkar Marg, Next to Mayor's, Dadar West, Shivaji Park, Mumbai, Maharashtra 400028",
    cell: "09833346047",
    email: "slimwell@example.com",
    ratings: 4,
    numReviews: 15,
    imgs: [
      {
        title: "Slimwell Dadar",
        contentType: "/images/jpg",
        imgPath: "/SlimwellDadar/SlimWell_Dadar_stock1.jpg",
      },
      {
        title: "Slimwell Dadar",
        contentType: "/images/jpg",
        imgPath: "/SlimwellDadar/SlimWell_Dadar_stock2.jpg",
      },
      {
        title: "Slimwell Dadar",
        contentType: "/images/jpg",
        imgPath: "/SlimwellDadar/SlimWell_Dadar_stock3.jpg",
      },
      {
        title: "Slimwell Dadar",
        contentType: "/images/jpg",
        imgPath: "/SlimwellDadar/SlimWell_Dadar_stock4.jpg",
      },
      {
        title: "Slimwell Dadar",
        contentType: "/images/jpg",
        imgPath: "/SlimwellDadar/SlimWell_Dadar_stock5.jpg",
      },
    ],
    numReviews: 11,
  },
  {
    name: "Slimwell Panvel",
    location:
      "https://www.google.com/maps/place/Slim+Well+Panvel/@18.9854728,73.1108403,17z/data=!3m1!5s0x3be7e8134afff095:0xf3717a17d7b531ee!4m15!1m8!3m7!1s0x3be7e8134ebdd537:0x7a7cccaebcba69ab!2sSlim+Well+Panvel!8m2!3d18.9853262!4d73.1109184!10e5!16s%2Fg%2F11gcwvk6qr!3m5!1s0x3be7e8134ebdd537:0x7a7cccaebcba69ab!8m2!3d18.9853262!4d73.1109184!16s%2Fg%2F11gcwvk6qr?entry=ttu",
    address: "Mominpada, Old Panvel, Panvel, Navi Mumbai, Maharashtra 410206",
    cell: "09321633373",
    email: "slimwell@example.com",
    ratings: 4,
    numReviews: 15,
    imgs: [
      {
        title: "Slimwell Panvel",
        contentType: "/images/jpg",
        imgPath: "/SlimwellPanvel/SlimWell_Panvel_stock1.jpg",
      },
      {
        title: "Slimwell Panvel",
        contentType: "/images/jpg",
        imgPath: "/SlimwellPanvel/SlimWell_Panvel_stock2.jpg",
      },
      {
        title: "Slimwell Panvel",
        contentType: "/images/jpg",
        imgPath: "/SlimwellPanvel/SlimWell_Panvel_stock3.jpg",
      },
      {
        title: "Slimwell Panvel",
        contentType: "/images/jpg",
        imgPath: "/SlimwellPanvel/SlimWell_Panvel_stock4.jpg",
      },
      {
        title: "Slimwell Panvel",
        contentType: "/images/jpg",
        imgPath: "/SlimwellPanvel/SlimWell_Panvel_stock5.jpg",
      },
    ],
    numReviews: 14,
  },
]

export default gyms
