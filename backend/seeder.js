import colors from "colors"
import dotenv from "dotenv"
import connectDB from "./config/db.js"
import users from "./data/users.js"
import gyms from "./data/gyms.js"
import memberships from "./data/memberships.js"
import Gym from "./models/gymModel.js"
import Membership from "./models/membershipModel.js"
import Measurement from "./models/measurementModel.js"
import User from "./models/userModel.js"

dotenv.config()

connectDB()

const importData = async () => {
  try {
    await User.deleteMany()
    await Gym.deleteMany()
    await Measurement.deleteMany()
    await Membership.deleteMany()

    const createdUsers = await User.insertMany(users)
    const createdGyms = await Gym.insertMany(gyms)
    const sampleUser = createdUsers[2]._id
    const sampleGym = createdGyms[0]._id

    const sampleMembership = memberships.map((membership) => {
      return { ...membership, user: sampleUser, gym: sampleGym }
    })

    await Membership.insertMany(sampleMembership)
    console.log("Data imported".green.inverse)
    process.exit()
  } catch (err) {
    console.error(`${err}`.red.inverse)
    process.exit(1)
  }
}

const destroyData = async () => {
  try {
    await User.deleteMany()
    await Gym.deleteMany()
    await Measurement.deleteMany()
    await Membership.deleteMany()

    console.log("Data Destroyed".red.inverse)
    process.exit()
  } catch (err) {
    console.error(`${err.message}`.red.inverse)
    process.exit(1)
  }
}

if (process.argv[2] === "-d") {
  destroyData()
} else {
  importData()
}
