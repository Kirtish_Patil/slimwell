import colors from "colors"
import express from "express"
import dotenv from "dotenv"
import connectDB from "./config/db.js"
import { errorHandler, notFound } from "./middlewares/errorMiddleware.js"
import userRoutes from "./routes/userRoutes.js"
import adminRoutes from "./routes/adminRoutes.js"
import gymRoutes from "./routes/gymRoute.js"
import trialRoutes from "./routes/trialRoutes.js"
import uploadRoutes from "./routes/uploadRoutes.js"
import membershipRoutes from "./routes/membershipRoutes.js"
import measurementRoutes from "./routes/measurementRoutes.js"
import path from "path"
import stripePackage from "stripe"
import cors from "cors"

dotenv.config()

connectDB()

const stripe = new stripePackage(
  "sk_test_51OPkQQSGUSEokzlgeas9gcd7Rl7kU6cHQemHTjyazdfDUpEdfkFY2eSbK646vuc8Vy3egDGvrGuE8tplKUSPWPY900fAE4C1Bu"
)

const app = express()
app.use(express.json())
app.use(
  cors({
    origin: "http://localhost:5173",
  })
)

app.use("/api/users", userRoutes)
app.use("/api/admins", adminRoutes)
app.use("/api/gyms", gymRoutes)
app.use("/api/trial", trialRoutes)
app.use("/api/uploads", uploadRoutes)
app.use("/api/memberships", membershipRoutes)
app.use("/api/measurements", measurementRoutes)
app.post("/create-checkout-session", async (req, res) => {
  // CHECKOUT REQUEST
  try {
    const { member } = req.body
    const { membership, userInfo } = member
    console.log(member)

    const session = await stripe.checkout.sessions.create({
      billing_address_collection: "required",
      payment_method_types: ["card"],
      line_items: [
        {
          price_data: {
            currency: "inr",
            product_data: {
              name: membership.type,
            },
            unit_amount: +membership.price * 100,
          },
          quantity: 1,
        },
      ],
      mode: "payment",
      customer_email: userInfo.user.email,
      client_reference_id: userInfo.user._id,
      success_url: `${process.env.CLIENT_URL}/success`,
      cancel_url: `${process.env.CLIENT_URL}/cancel`,
    })

    res.json({ id: session.id, url: session.url })
  } catch (err) {
    res.status(500)
    res.json({ error: err.message })
  }
})

const __dirname = path.resolve()
app.use("/uploads", express.static(path.join(__dirname, "/uploads")))

if (process.env.NODE_ENV === "production") {
  app.use(express.static(path.join(__dirname, "/frontend/dist")))

  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "frontend", "dist", "index.html"))
  })
} else {
  app.get("/", (req, res) => {
    res.send("Api is running.....")
  })
}

app.use(notFound)
app.use(errorHandler)

const PORT = process.env.PORT || 7000

app.listen(PORT, () => {
  console.log(
    `Server running in ${process.env.NODE_ENV} mode on port ${process.env.PORT}`
      .yellow.inverse
  )
})
