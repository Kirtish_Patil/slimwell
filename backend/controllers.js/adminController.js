import asyncHandler from "express-async-handler"
import User from "../models/userModel.js"
import generateToken from "../utils/generateToken.js"

/**
 * @desc    Login an admin
 * @route   Post /api/users/loginadmin
 * @access  public
 */

const authAdmin = asyncHandler(async (req, res) => {
  const { email, password, verificationKey } = req.body

  const admin = await User.findOne({ email })

  const adminAuthCondition =
    verificationKey !== "slimwell_admin" &&
    verificationKey !== "slimwell_superAdmin"

  if (admin.role === "user") {
    res.status(401)
    throw new Error("Not yet authorised")
  }

  if (adminAuthCondition) {
    res.status(401)
    throw new Error("Authentication Failed. Try again")
  }

  if (verificationKey === "slimwell_superAdmin") {
    if (admin.role !== "superAdmin") {
      res.status(401)
      throw new Error("Access Denied. Try again")
    }
  }

  if (admin && (await admin.matchPassword(password))) {
    res.status(201)
    res.json({
      _id: admin._id,
      name: admin.name,
      email: admin.email,
      role: admin.role,
      token: generateToken(admin._id),
    })
  } else {
    res.status(404)
    throw new Error("Invalid email or password")
  }
})

const registerAdmin = asyncHandler(async (req, res) => {
  const { name, email, password, position, verificationKey } = req.body

  const adminExists = await User.findOne({ email })

  if (adminExists) {
    res.status(400)
    throw new Error("Account already exists. Try Logging In")
  }

  if (position === "owner") {
    if (verificationKey !== "slimwell_superAdmin") {
      res.status(401)
      throw new Error("Unauthorised.")
    }
  }

  const admin = await User.create({
    name,
    email,
    password,
    position,
    verificationKey,
  })

  if (admin) {
    res.status(201)
    if (position === "owner") {
      res.json({
        _id: admin._id,
        name: admin.name,
        email: admin.email,
        role: "superAdmin",
        position: admin.position,
        token: generateToken(admin._id),
      })
    } else {
      res.json({
        _id: admin._id,
        name: admin.name,
        email: admin.email,
        role: "user",
        position: admin.position,
        token: generateToken(admin._id),
      })
    }
  } else {
    res.status(400)
    res.json("Invalid data submitted.")
  }
})

export { authAdmin, registerAdmin }
