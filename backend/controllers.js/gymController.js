import asyncHandler from "express-async-handler"
import Gym from "../models/gymModel.js"

/**
 * @desc     Get all gyms
 * @route    Get  /api/gyms
 * @access   private
 */

const getAllGyms = asyncHandler(async (req, res) => {
  const gyms = await Gym.find({})
  res.json(gyms)
})

export { getAllGyms }
