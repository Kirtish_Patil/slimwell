import Trial from "../models/trialModel.js"
import asyncHandler from "express-async-handler"
import dayjs from "dayjs"

/**
 * @desc        Create a Trial
 * @route       Post /api/trial
 * @access      private
 */

const createTrial = asyncHandler(async (req, res) => {
  const { user, gym } = req.body

  const existTrial = await Trial.findOne({ user })

  let startDate = dayjs().format("DD/MM/YYYY")
  let endDate = dayjs().add(8, "day").format("DD/MM/YYYY")

  if (existTrial) {
    res.status(201)
    res.json(existTrial)
  } else {
    const trial = new Trial({
      user,
      gym,
      startDate,
      endDate,
      isActive: true,
    })

    const createdTrial = await trial.save()
    res.status(201)
    res.json(createdTrial)
  }
})

/**
 * @desc    Get trial by Id
 * @route   Get /api/trial/:id
 * @access  private
 */

const getTrialById = asyncHandler(async (req, res) => {
  const trial = await Trial.findById(req.params.id)
    .populate("gym")
    .populate("user", "-password -verificationKey")

  if (trial) {
    res.status(201)
    res.json(trial)
  } else {
    res.status(400)
    throw new Error("Something went wrong. Try again")
  }
})

export { createTrial, getTrialById }
