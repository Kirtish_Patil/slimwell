import asyncHandler from "express-async-handler"
import User from "../models/userModel.js"
import generateToken from "../utils/generateToken.js"

/**
 * @desc    Get all users
 * @route   Get /api/users/
 * @access  private/admin
 */

const getAllUsers = asyncHandler(async (req, res) => {
  const users = await User.find({}).select("-password")
  res.json(users)
})

/**
 * @desc    Get User By ID
 * @route   Get /api/users/:id
 * @access  private
 */

const getUserById = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id)
  if (user) {
    res.json(user)
  } else {
    res.status(404).json({
      message: "Product not found",
    })
  }
})

/**
 * @desc    Login a user
 * @route   Post /api/users/login
 * @access  private
 */

const authUser = asyncHandler(async (req, res) => {
  const { email, password } = req.body
  const user = await User.findOne({ email })

  const loginCondition = user && (await user.matchPassword(password))

  if (loginCondition) {
    const userData = await User.findOne({ email }).select("-password")

    const token = generateToken(user._id)

    const userDataWithToken = {
      ...userData.toObject(),
      token: token,
    }

    res.json(userDataWithToken)
  } else {
    res.status(404)
    throw new Error(`Invalid email or password`)
  }
})

/**
 * @desc    Register a User
 * @route   Post /api/users
 * @access   public
 */

const registerUser = asyncHandler(async (req, res) => {
  const { name, email, password } = req.body

  const userExists = await User.findOne({ email })

  if (userExists) {
    res.status(400) //Bad request
    throw new Error("User already exists")
  }

  const user = await User.create({ name, email, password })

  if (user) {
    res.status(201) //successfully created
    res.json({
      _id: user._id,
      name: user.name,
      email: user.email,
      role: user.role,
      token: generateToken(user._id),
    })
  } else {
    res.status(400)
    throw new Error("Invalid user data")
  }
})

/**
 * @desc    update a user
 * @route   Put /api/users
 * @access  private
 */

const updateUser = asyncHandler(async (req, res) => {
  const { name, email, phone, address, age, gender, image } = req.body

  const user = await User.findById(req.user._id)

  if (user) {
    const updatedUserInfo = {}

    if (phone) updatedUserInfo.phone = phone
    if (address) updatedUserInfo.address = address
    if (age) updatedUserInfo.age = age
    if (gender) updatedUserInfo.gender = gender
    if (image) updatedUserInfo.image = image

    await User.updateOne({ _id: req.user }, { $set: updatedUserInfo })

    const updatedUser = await User.findById(req.user._id).select("-password")

    res.status(200)
    res.json({
      name: updatedUser.name,
      email: updatedUser.email,
      phone: updatedUser.phone,
      address: updatedUser.address,
      age: updatedUser.address,
      gender: updatedUser.gender,
      image: updatedUser.image,
    })
    console.log("i ran")
  } else {
    res.status(500)
    throw new Error("Failed to update user")
  }
})

/**
 * @desc    Verify User email
 * @route   Post /api/users/verify
 * @access  Public
 */

// const verifyMail = asyncHandler(async(req, res) => {
//   const {email}

// })

export { getAllUsers, getUserById, authUser, registerUser, updateUser }
