import asyncHandler from "express-async-handler"
import Measurement from "../models/measurementModel.js"

/**
 * @desc    Create a Measurement
 * @route   Post /api/measurements
 * @access  private
 */

const createMeasurement = asyncHandler(async (req, res) => {
  const { userId, measurements } = req.body
  console.log(req.body)
  
  const existsMeasurement = await Measurement.findOne({ userId })
  
  if (existsMeasurement) {
    res.status(201)
    res.json(existsMeasurement)
  }
  
  const measurement = await Measurement.create({
    user: userId,
    weight: measurements.weight,
    neckCircumference: measurements.neckCircumference,
    chestRelaxed: measurements.chestRelaxed,
    chestExpanded: measurements.chestExpanded,
    waistRelaxed: measurements.waistRelaxed,
    waistExpanded: measurements.waistExpanded,
    bicepsRelaxed: measurements.bicepsRelaxed,
    bicepsExpanded: measurements.bicepsExpanded,
    forearms: measurements.forearms,
    thighs: measurements.thighs,
    calfs: measurements.calfs,
    wrist: measurements.wrist,
  })

  if (measurement) {
    const createdMeasurement = await measurement.save()
    res.status(201)
    res.json(createdMeasurement)
    console.log(createdMeasurement)
  } else {
    res.status(400)
    throw new Error("Invalid data")
  }
})

export { createMeasurement }
