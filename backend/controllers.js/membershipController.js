import Membership from "../models/membershipModel.js"
import asyncHandler from "express-async-handler"

/**
 * @desc    Create a membership
 * @route   Post /api/memberships/
 * @access  private
 */
