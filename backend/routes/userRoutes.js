import express from "express"
import {
  authUser,
  getAllUsers,
  getUserById,
  registerUser,
  updateUser,
} from "../controllers.js/userControllers.js"
import { protect } from "../middlewares/authMiddleware.js"

const router = express.Router()

router
  .route("/")
  .post(registerUser)
  .get(protect, getAllUsers)
  .put(protect, updateUser)
router.route("/:id").get(getUserById)
router.route("/loginuser").post(authUser)

export default router
