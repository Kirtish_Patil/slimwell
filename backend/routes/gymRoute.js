import express from "express"
import { protect } from "../middlewares/authMiddleware.js"
import { getAllGyms } from "../controllers.js/gymController.js"

const router = express.Router()

router.route("/").get(protect, getAllGyms)

export default router
