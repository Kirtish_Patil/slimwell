import express from "express"
import { authAdmin, registerAdmin } from "../controllers.js/adminController.js"

const router = express.Router()

router.route("/").post(registerAdmin)
router.route("/loginadmin").post(authAdmin)

export default router
