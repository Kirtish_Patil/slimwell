import express from "express"
import { protect } from "../middlewares/authMiddleware.js"
import { createMeasurement } from "../controllers.js/measurementController.js"

const router = express.Router()

router.route("/").post(protect, createMeasurement)

export default router
