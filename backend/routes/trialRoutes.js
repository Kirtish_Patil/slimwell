import express from "express"
import { protect } from "../middlewares/authMiddleware.js"
import { createTrial, getTrialById } from "../controllers.js/trialController.js"

const router = express.Router()

router.route("/").post(protect, createTrial)
router.route("/:id").get(protect, getTrialById)

export default router
