export const gyms = [
  {
    imgs: [
      {
        title: "Slimwell Fitness Club",
        contentType: "/images/jpg",
        imgPath: "/SlimwellFitnessClub/SlimWell_Mahim_stock1.jpg",
      },
      {
        title: "Slimwell Fitness Club",
        contentType: "/images/jpg",
        imgPath: "/SlimwellFitnessClub/SlimWell_Mahim_stock2.jpg",
      },
      {
        title: "Slimwell Fitness Club",
        contentType: "/images/jpg",
        imgPath: "/SlimwellFitnessClub/SlimWell_Mahim_stock3.jpg",
      },
      {
        title: "Slimwell Fitness Club",
        contentType: "/images/jpg",
        imgPath: "/SlimwellFitnessClub/SlimWell_Mahim_stock4.jpg",
      },
      {
        title: "Slimwell Fitness Club",
        contentType: "/images/jpg",
        imgPath: "/SlimwellFitnessClub/SlimWell_Mahim_stock5.jpg",
      },
    ],
  },
  {
    imgs: [
      {
        title: "Slimwell Dadar",
        contentType: "/images/jpg",
        imgPath: "/SlimwellDadar/SlimWell_Dadar_stock1.jpg",
      },
      {
        title: "Slimwell Dadar",
        contentType: "/images/jpg",
        imgPath: "/SlimwellDadar/SlimWell_Dadar_stock2.jpg",
      },
      {
        title: "Slimwell Dadar",
        contentType: "/images/jpg",
        imgPath: "/SlimwellDadar/SlimWell_Dadar_stock3.jpg",
      },
      {
        title: "Slimwell Dadar",
        contentType: "/images/jpg",
        imgPath: "/SlimwellDadar/SlimWell_Dadar_stock4.jpg",
      },
      {
        title: "Slimwell Dadar",
        contentType: "/images/jpg",
        imgPath: "/SlimwellDadar/SlimWell_Dadar_stock5.jpg",
      },
    ],
  },
  {
    imgs: [
      {
        title: "Slimwell Panvel",
        contentType: "/images/jpg",
        imgPath: "/SlimwellPanvel/SlimWell_Panvel_stock1.jpg",
      },
      {
        title: "Slimwell Panvel",
        contentType: "/images/jpg",
        imgPath: "/SlimwellPanvel/SlimWell_Panvel_stock2.jpg",
      },
      {
        title: "Slimwell Panvel",
        contentType: "/images/jpg",
        imgPath: "/SlimwellPanvel/SlimWell_Panvel_stock3.jpg",
      },
      {
        title: "Slimwell Panvel",
        contentType: "/images/jpg",
        imgPath: "/SlimwellPanvel/SlimWell_Panvel_stock4.jpg",
      },
      {
        title: "Slimwell Panvel",
        contentType: "/images/jpg",
        imgPath: "/SlimwellPanvel/SlimWell_Panvel_stock5.jpg",
      },
    ],
  },
]
