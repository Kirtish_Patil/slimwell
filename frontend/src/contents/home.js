const main = {
  title: "Quality Equipments at you doorstep",
  subTitle: "Explore a new level of fitness with",
  description: [
    "High-quality materials for durability",
    "Customizable options to meet your specific needs",
    "Affordable rates for every budget",
    "Expert guidance for selecting the right equipment",
  ],
  image: "/source/equipments.jpg",
  gymImagePath : `/source/Gym.jpg`,
}

export {main}
