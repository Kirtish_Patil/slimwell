const instuctions = [
  "Clean Up After Yourself: Wipe down equipment after use.",
  "Respect Others: Re-rack weights and share equipment.",
  "Use Towels: Bring and use towels during workouts.",
  "Stay Safe: Follow safety instructions on machines.",
  "Ask for Help: Our staff is here to assist you.",
  "Keep it Safe: Avoid overcrowding and maintain distancing.",
  "Report Maintenance: Notify staff of any equipment issues.",
  "Membership Queries: Approach the front desk for assistance.",
  "Feedback Matters: Share your feedback for improvements.",
  "Have Fun and Enjoy Your Workout!",
]


export default instuctions