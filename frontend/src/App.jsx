import Header from "./components/Header"
import Footer from "./components/Footer"
import { BrowserRouter, Routes, Route } from "react-router-dom"
import { CssBaseline, ThemeProvider, createTheme } from "@mui/material"
import theme from "./theme"
import HomeScreen from "./screens/HomeScreen"
import FreeTrialScreen from "./screens/FreeTrialScreen"
import MembershipScreen from "./screens/MembershipScreen"
import LoginUserScreen from "./screens/LoginUserScreen"
import UserRegisterScreen from "./screens/UserRegisterScreen"
import LoginAdmin from "./screens/LoginAdmin"
import AdminRegisterScreen from "./screens/AdminRegisterScreen"
import AdminInviteScreen from "./screens/AdminInviteScreen"
import TrialSuccessScreen from "./screens/TrialSuccessScreen"
import PlanRegisterScreen from "./screens/PlanRegisterScreen"
import CheckoutScreen from "./screens/CheckoutScreen"
import PaymentScreen from "./screens/PaymentScreen"
import ProfileScreen from "./screens/ProfileScreen"
import MeasurementsScreen from "./screens/MeasurementsScreen"

const App = () => {
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Header />
        <Routes>
          <Route path="/" element={<HomeScreen />} />
          <Route path="/loginuser" element={<LoginUserScreen />} />
          <Route path="/loginadmin" element={<LoginAdmin />} />
          <Route path="/trial" element={<FreeTrialScreen />} />
          <Route path="/membership" element={<MembershipScreen />} />
          <Route path="/registeruser" element={<UserRegisterScreen />} />
          <Route path="/registeradmin" element={<AdminRegisterScreen />} />
          <Route path="/admininvite" element={<AdminInviteScreen />} />
          <Route path="/trialsuccess/:id" element={<TrialSuccessScreen />} />
          <Route path="/planregister" element={<PlanRegisterScreen />} />
          <Route path="/checkout" element={<CheckoutScreen />} />
          <Route path="/payment" element={<PaymentScreen />} />
          <Route path="/profile" element={<ProfileScreen />} />
          <Route path="/measurements" element={<MeasurementsScreen />} />
        </Routes>
        <Footer />
      </ThemeProvider>
    </BrowserRouter>
  )
}

export default App
