import { styled, Button, Box, TextField, Typography } from "@mui/material"

const StyledButton = styled(Button)(({ theme }) => ({
  color: theme.palette.primary.contrastText,
  borderRadius: "0",
  padding: "1rem",
  textTransform: "uppercase",
  fontWeight: "bold",
  textDecoration: "none",
}))

const StyledButtonSecondary = styled(Button)(({ theme }) => ({
  backgroundColor: theme.palette.secondary.main,
  color: "white",
  borderRadius: "6px",
  transition: "background-color 0.3s",
  textDecoration: "none",
  ":hover": { backgroundColor: theme.palette.secondary.light },
}))

const FormContainer = styled(Box)(({ theme }) => ({
  display: "flex",
  flexDirection: "column",
  // alignItems: "flex-start",
  justifyContent: "center",
  height: "100%",
  width: "100%",
  gap: "1rem",
  padding: "2rem",
  margin: "5rem 0rem",
  border: "2px",
  borderStyle: "solid",
  borderColor: "rgba(0,0,0,0.1)",
  borderRadius: "10px",
}))

const TextList = styled(Typography)(({ theme }) => ({
  textAlign: "start",
  fontWeight: "600",
  fontSize: "18px",
  marginTop: "0.5rem",
  marginBottom: "0.5rem",
}))

const StyledTextField = styled(TextField)(({ theme }) => ({}))

export {
  StyledButton,
  StyledButtonSecondary,
  FormContainer,
  StyledTextField,
  TextList,
}
