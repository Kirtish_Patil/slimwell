import {
  Grid,
  Box,
  Paper,
  Typography,
  Button,
  List,
  ListItem,
  ListItemText,
} from "@mui/material"
import { Link as RouterLink } from "react-router-dom"
import React, { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useNavigate, useParams } from "react-router-dom"
import { getTrialDetails } from "../actions/trialActions"
import Loader from "../components/Loader"
import Message from "../components/Message"
import AccountCircleIcon from "@mui/icons-material/AccountCircle"
import theme from "../theme"
import instuctions from "../contents/instuctions.js"
import { StyledButtonSecondary } from "../muiCustom/customComponents.jsx"

const TrialSuccessScreen = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const { id: trialId } = useParams()

  const trialDetails = useSelector((state) => state.trialDetails)
  const { loading, error, trial } = trialDetails

  const userLogin = useSelector((state) => state.userLogin)
  const { userInfo, loading: userLoading } = userLogin

  useEffect(() => {
    window.scroll(0, 0)
    if (userInfo) {
      dispatch(getTrialDetails(trialId))
    } else {
      navigate("/loginuser")
    }
  }, [userInfo, trialId])
  return (
    <Grid display="flex" paddingY="3rem" paddingX="2.2rem">
      {(loading || userLoading) && <Loader />}
      {error && <Message type="error">{error}</Message>}

      {trial.user && trial.gym && (
        <Grid
          container
          width="100%"
          justifyContent="center"
          alignItems="center"
          direction="column"
          gap="1rem"
        >
          <Box textAlign="flex-start" width="100%">
            <Typography variant="h4" fontWeight="300" width="100%">
              Congratulations
            </Typography>
            <Typography variant="p" fontWeight="300" width="100%">
              Your Free Trial is Ready. To claim your trial for a different Gym,
              cancel your existing trial first
            </Typography>
          </Box>

          {/* Trial Card */}
          <Paper
            sx={{
              width: { xs: "90%", sm: "60%" },
              marginTop: "1rem",
              padding: "2rem",
              bgcolor: "white",
            }}
          >
            <AccountCircleIcon
              fontSize="2rem"
              sx={{
                width: "100px",
                height: "100px",
                color: theme.palette.primary.dark,
                marginY: "0.5rem",
              }}
            />
            <Typography variant="h5" fontWeight="700">
              Hii {trial.user.name}
            </Typography>
            <Typography variant="h6">
              Enjoy your Free Trial Between {trial.startDate} to {trial.endDate}
            </Typography>
            <Typography component="p">{trial.gym.name}</Typography>
            <StyledButtonSecondary
              sx={{
                marginY: "0.5rem",
                bgcolor: theme.palette.secondary.main,
                color: "white",
              }}
              onClick={() => window.open(trial.gym.location)}
            >
              Location
            </StyledButtonSecondary>
            <Typography>{trial.gym.address}</Typography>
            <Typography>Contact: {trial.gym.cell}</Typography>
            <Typography>Email: {trial.gym.email}</Typography>
          </Paper>

          {/* Instuctions */}
          <Box width="100%" textAlign="flex-start">
            <Typography variant="h4">Instructions</Typography>

            {instuctions.map((inst, idx) => (
              <List component="ol">
                <ListItem key={idx}>
                  <ListItemText primary={`${idx + 1}. ${inst}`} />
                </ListItem>
              </List>
            ))}
          </Box>
          <Button
            variant="outlined"
            as={RouterLink}
            to="/"
            sx={{
              borderColor: theme.palette.secondary.main,
              color: "black",
              width: "100%",
              textAlign: "center",
              textDecoration: "none",
            }}
          >
            Home
          </Button>
        </Grid>
      )}
    </Grid>
  )
}

export default TrialSuccessScreen
