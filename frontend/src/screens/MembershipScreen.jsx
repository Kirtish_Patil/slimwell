import {
  Card,
  CardContent,
  CardHeader,
  Container,
  Grid,
  Typography,
  Box,
} from "@mui/material"
import {
  StyledButtonSecondary,
  TextList,
} from "../muiCustom/customComponents.jsx"
import CheckIcon from "@mui/icons-material/Check"
import ClearIcon from "@mui/icons-material/Clear"
import React, { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import { createMembership } from "../actions/membershipActions.js"
import memberships from "../../../backend/data/memberships.js"

const MembershipScreen = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const userLogin = useSelector((state) => state.userLogin)
  const { userInfo } = userLogin

  const handleCreateMembership = (e, mem) => {
    e.preventDefault()
    dispatch(createMembership(mem))
    navigate("/planregister")
  }

  useEffect(() => {
    if (!userInfo) {
      navigate("/loginuser")
    }
  }, [userInfo])

  return (
    <div>
      <Container
        disableGutters
        component="main"
        sx={{ paddingY: 8, paddingX: 5 }}
      >
        <Typography variant="h3" component="h2" align="center" gutterBottom>
          Plans
        </Typography>
        <Typography variant="h5" align="center" component="p" color="gray">
          Explore a neighborhood gym built for you, offering budget-friendly
          membership options that cater to all fitness levels and goals.
        </Typography>
      </Container>

      {/* Membership Plans */}
      <Container maxWidth="xl" component="main">
        <Grid container spacing={3} justifyContent="center">
          {memberships.map((mem, idx) => (
            <Grid
              item
              key={idx}
              xs={12}
              sm={6}
              md={6}
              lg={3}
              mb={5}
              justifyContent="center"
              alignItems="center"
            >
              <Card
                elevation={2}
                sx={{
                  textAlign: "center",
                  background:
                    mem.type === "Basic"
                      ? "linear-gradient(45deg, #dbdad0, #f4f3e8)"
                      : mem.type === "Silver"
                      ? "linear-gradient(45deg, #6f6e65, #dbdad0)"
                      : mem.type === "Golden"
                      ? "linear-gradient(45deg, #BF953F, #FCF6BA)"
                      : mem.type === "Platinum"
                      ? "linear-gradient(45deg, #52535c, #d1d3dd)"
                      : "white",
                  ":hover": { transform: "scale(1.05)" },
                  transition: "transform 0.1s ease",
                }}
              >
                <CardHeader
                  title={mem.type}
                  subheader={mem.duration}
                  sx={{ mt: "1rem" }}
                />
                <Typography component="h2" variant="h4" fontWeight="600">
                  ₹{mem.price}
                </Typography>
                <CardContent>
                  <Box
                    sx={{
                      paddingBottom: "2rem",
                      paddingTop: "1rem",
                      paddingX: "0.2rem",
                    }}
                  >
                    {mem.benefits.map((benifit, idx) => (
                      <Box>
                        {mem.type === "Basic" && idx < 1 ? (
                          <TextList>
                            <CheckIcon sx={{ mr: "0.2rem" }} />
                            {benifit}
                          </TextList>
                        ) : mem.type === "Silver" && idx < 2 ? (
                          <TextList>
                            <CheckIcon sx={{ mr: "0.2rem" }} />
                            {benifit}
                          </TextList>
                        ) : mem.type === "Golden" && idx < 3 ? (
                          <TextList>
                            <CheckIcon sx={{ mr: "0.2rem" }} />
                            {benifit}
                          </TextList>
                        ) : mem.type === "Platinum" ? (
                          <TextList>
                            <CheckIcon sx={{ mr: "0.2rem" }} />
                            {benifit}
                          </TextList>
                        ) : (
                          <Typography
                            textAlign="start"
                            variant="h6"
                            fontWeight="300"
                            fontSize={18}
                            marginY="0.5rem"
                          >
                            <ClearIcon sx={{ mr: "0.2rem" }} />
                            {benifit}
                          </Typography>
                        )}
                      </Box>
                    ))}
                  </Box>
                  <StyledButtonSecondary
                    onClick={(e) => handleCreateMembership(e, mem)}
                    sx={{ width: "100%" }}
                  >
                    Select Plan
                  </StyledButtonSecondary>{" "}
                </CardContent>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>
    </div>
  )
}

export default MembershipScreen
