import { Box, Stack } from "@mui/material"
import React, { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import Message from "../components/Message"
import { listGyms } from "../actions/gymsActions"
import Loader from "../components/Loader"
import GymCaraousel from "../components/GymCaraousel"
import { StyledButtonSecondary } from "../muiCustom/customComponents"
import { logout } from "../actions/userActions"

const FreeTrialScreen = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const userLogin = useSelector((state) => state.userLogin)
  const { userInfo } = userLogin

  const adminLogin = useSelector((state) => state.adminLogin)
  const { adminInfo } = adminLogin

  const gymsList = useSelector((state) => state.gymsList)
  const { loading, error, gyms } = gymsList

  const trialCreate = useSelector((state) => state.trialCreate)
  const { error: trialError, trial, success } = trialCreate

  useEffect(() => {
    window.scroll(0, 0)
    if (userInfo) {
      dispatch(listGyms())
    } else {
      navigate("/loginuser")
    }
  }, [userInfo])

  const failedTokenHandler = () => {
    dispatch(logout())
    navigate("/loginuser")
  }
  return (
    <Box
      sx={{
        justifyContent: "center",
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <Stack
        sx={{
          width: { xs: "90%", sm: "80%" },
          flexDirection: "column",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {loading ? (
          <Loader />
        ) : error ? (
          <Box
            width="100%"
            my="1rem"
            display="flex"
            flexDirection="column"
            alignItems="center"
            gap={2}
          >
            <Message type="error">{error}</Message>
            <StyledButtonSecondary onClick={failedTokenHandler}>
              Login
            </StyledButtonSecondary>
          </Box>
        ) : (
          <div>
            {userInfo && (
              <Stack key={1}>
                {trialError && <Message type="error">{trialError}</Message>}
                {gyms.map((gym, idx) => (
                  <GymCaraousel gym={gym} idx={idx} />
                ))}
              </Stack>
            )}
          </div>
        )}
      </Stack>
    </Box>
  )
}

export default FreeTrialScreen
