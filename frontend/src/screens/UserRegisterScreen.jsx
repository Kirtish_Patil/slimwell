import { Box, Link, Stack, Typography } from "@mui/material"
import {
  FormContainer,
  StyledButtonSecondary,
  StyledTextField,
} from "../muiCustom/customComponents.jsx"
import { useDispatch, useSelector } from "react-redux"
import {
  Link as RouterLink,
  useNavigate,
  useSearchParams,
} from "react-router-dom"
import { useEffect, useState } from "react"
import { registerUser } from "../actions/userActions.js"
import Message from "../components/Message.jsx"
import AuthNav from "../components/AuthNav.jsx"

const UserRegisterScreen = () => {
  const dispath = useDispatch()
  const navigate = useNavigate()

  const [searchParams] = useSearchParams()
  let redirect = searchParams.get("redirect") || "/loginuser"

  const userRegister = useSelector((state) => state.userRegister)
  const { loading, error, userInfo } = userRegister

  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [confirmPassword, setConfirmPassword] = useState("")
  const [message, setMessage] = useState("")

  const submitHandler = (e) => {
    e.preventDefault()
    window.scroll(0, 0)
    if (password !== confirmPassword) {
      setMessage("Passwords did not match")
    } else {
      dispath(registerUser(name, email, password))
      navigate("/loginuser")
    }
  }

  useEffect(() => {
    if (userInfo) {
      navigate(redirect)
    }
  }, [userInfo, redirect])

  return (
    <Stack
      direction="row"
      sx={{
        justifyContent: "center",
        alignItems: "center",
        display: "flex",
      }}
    >
      <Box
        sx={{
          width: { xs: "90vw", sm: "70vw", md: "40vw" },
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
        }}
      >
        <FormContainer component="form" onSubmit={submitHandler}>
          <AuthNav user="/registeruser" admin="/registeradmin" />
          {error && <Message type="error">{error}</Message>}
          {message && <Message type="error">{message}</Message>}
          <Typography variant="h4" component="h2">
            User Register
          </Typography>

          <Stack width="100%">
            Name
            <StyledTextField
              placeholder="Name"
              required
              fullWidth
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </Stack>

          <Stack width="100%">
            Email
            <StyledTextField
              placeholder="Email"
              required
              fullWidth
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Stack>

          <Stack width="100%">
            Password
            <StyledTextField
              placeholder="Password"
              type="password"
              required
              fullWidth
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </Stack>

          <Stack width="100%">
            Confirm Password
            <StyledTextField
              placeholder="Re-enter you password"
              type="password"
              required
              fullWidth
              value={confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
            />
          </Stack>

          {/* <Stack width="100%">
            <FormControlLabel
              control={<Checkbox color="secondary" />}
              label="Admin ?"
              onChange={(e) => setIsAdmin(e.target.checked)}
              checked={isAdmin}
            />
            Secure Key
            <StyledTextField
              placeholder="Enter Admin secure key if Admin"
              fullWidth
              value={secretyKey}
              onChange={(e) => setSecretKey(e.target.value)}
            />
          </Stack> */}

          <Stack
            direction="column"
            spacing={2}
            sx={{ justifyContent: "center", alignItems: "flex-start" }}
          >
            <StyledButtonSecondary type="submit">
              Register
            </StyledButtonSecondary>{" "}
            <Typography variant="p">
              Already a Member?{" "}
              <Link
                component={RouterLink}
                to="/loginuser"
                sx={{ color: "black", fontWeight: "bold" }}
              >
                Click here to Login
              </Link>
            </Typography>
          </Stack>
        </FormContainer>
      </Box>
    </Stack>
  )
}

export default UserRegisterScreen
