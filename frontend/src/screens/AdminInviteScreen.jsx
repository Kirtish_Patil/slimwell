import React from "react"
import { Link as RouterLink } from "react-router-dom"
import {
  FormContainer,
  StyledButtonSecondary,
} from "../muiCustom/customComponents"
import { Box, Icon, Typography } from "@mui/material"
import CheckCircleIcon from "@mui/icons-material/CheckCircle"
import { useSelector } from "react-redux"

const AdminInviteScreen = () => {
  return (
    <Box sx={{ display: "flex", justifyContent: "center" }}>
      <FormContainer
        sx={{
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          textAlign: "center",
        }}
      >
        <CheckCircleIcon
          color="success"
          sx={{ width: "100px", height: "100px" }}
        />
        <Typography variant="h5" component="h4">
          Your Application Has Been Submitted
        </Typography>
        <Typography variant="p" component="p">
          Your application will be reviewed soon. Till then enjoy our
          application by loggin in as a user. Check your login access after few
          hours in admin login.
        </Typography>
        <StyledButtonSecondary
          as={RouterLink}
          to="/loginuser"
          sx={{ padding: "0.2rem 1rem", mt: "1rem" }}
        >
          Login
        </StyledButtonSecondary>
      </FormContainer>
    </Box>
  )
}

export default AdminInviteScreen
