import {
  Box,
  InputAdornment,
  Stack,
  TextField,
  Typography,
} from "@mui/material"
import React, { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import {
  FormContainer,
  StyledButtonSecondary,
} from "../muiCustom/customComponents"
import { createMeasurement } from "../actions/measurementActions"

const MeasurementsScreen = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const [weight, setWeight] = useState(0)
  const [neckCir, setNeckCir] = useState(0)
  const [chestRelaxed, setChestRelaxed] = useState(0)
  const [chestExpanded, setChestExpanded] = useState(0)
  const [waistRelaxed, setWaistRelaxed] = useState(0)
  const [waistExpanded, setWaistExpanded] = useState(0)
  const [bicepsRelaxed, setBicepsRelaxed] = useState(0)
  const [bicepsExpanded, setBicepsExpanded] = useState(0)
  const [forearms, setForearms] = useState(0)
  const [thighs, setThighs] = useState(0)
  const [calfs, setCalfs] = useState(0)
  const [wrist, setWrist] = useState(0)

  const userLogin = useSelector((state) => state.userLogin)
  const { userInfo } = userLogin

  const measurementCreate = useSelector((state) => state.measurementCreate)
  const { loading, error, measurement } = measurementCreate
  console.log(measurementCreate)

  const submitHandler = (e) => {
    e.preventDefault()
    dispatch(
      createMeasurement(userInfo._id, {
        weight,
        neckCircumference: neckCir,
        chestRelaxed,
        chestExpanded,
        waistRelaxed,
        waistExpanded,
        bicepsRelaxed,
        bicepsExpanded,
        forearms,
        thighs,
        calfs,
        wrist,
      })
    )
  }

  useEffect(() => {
    if (userInfo) {
      if (measurement) {
        setWeight(measurement.weight)
        setNeckCir(measurement.neckCircumference)
        setChestRelaxed(measurement.chestRelaxed)
        setChestExpanded(measurement.chestExpanded)
        setWaistRelaxed(measurement.waistRelaxed)
        setWaistExpanded(measurement.waistExpanded)
        setBicepsRelaxed(measurement.bicepsRelaxed)
        setBicepsExpanded(measurement.bicepsExpanded)
        setForearms(measurement.forearms)
        setThighs(measurement.thighs)
        setCalfs(measurement.calfs)
        setWrist(measurement.wrist)
      }
    } else {
      navigate("/loginuser")
    }
  })

  return (
    <Stack sx={{ alignItems: "center" }}>
      <Box
        sx={{
          width: { xs: "90vw", sm: "70vw", md: "40vw" },
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
        }}
      >
        <FormContainer component="form" onSubmit={submitHandler}>
          <Typography variant="h4" component="p">
            Measurements
          </Typography>
          <Typography variant="p" component="p">
            Measure and track your physique with our App
          </Typography>

          <Stack width="45%">
            Weight
            <TextField
              type="number"
              placeholder="Weight"
              value={weight}
              onChange={(e) => setWeight(e.target.value)}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">kg</InputAdornment>
                ),
              }}
            />
          </Stack>

          <Stack width="45%">
            Neck Circumference
            <TextField
              type="number"
              placeholder="Weight"
              value={neckCir}
              onChange={(e) => setNeckCir(e.target.value)}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">cm</InputAdornment>
                ),
              }}
            />
          </Stack>

          <Stack width="100%" direction="row" justifyContent="space-between">
            <Stack width="45%">
              Chest Relaxed
              <TextField
                type="number"
                placeholder="Weight"
                value={chestRelaxed}
                onChange={(e) => setChestRelaxed(e.target.value)}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">cm</InputAdornment>
                  ),
                }}
              />
            </Stack>

            <Stack>
              Chest Expanded
              <TextField
                type="number"
                placeholder="Weight"
                value={chestExpanded}
                onChange={(e) => setChestExpanded(e.target.value)}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">cm</InputAdornment>
                  ),
                }}
              />
            </Stack>
          </Stack>

          <Stack width="100%" direction="row" justifyContent="space-between">
            <Stack width="45%">
              Waist Relaxed
              <TextField
                type="number"
                placeholder="Weight"
                value={waistRelaxed}
                onChange={(e) => setWaistRelaxed(e.target.value)}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">cm</InputAdornment>
                  ),
                }}
              />
            </Stack>

            <Stack>
              Waist Expanded
              <TextField
                type="number"
                placeholder="Weight"
                value={waistExpanded}
                onChange={(e) => setWaistExpanded(e.target.value)}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">cm</InputAdornment>
                  ),
                }}
              />
            </Stack>
          </Stack>

          <Stack width="100%" direction="row" justifyContent="space-between">
            <Stack width="45%">
              Biceps Relaxed
              <TextField
                type="number"
                placeholder="Weight"
                value={bicepsRelaxed}
                onChange={(e) => setBicepsRelaxed(e.target.value)}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">cm</InputAdornment>
                  ),
                }}
              />
            </Stack>

            <Stack>
              Biceps Expanded
              <TextField
                type="number"
                placeholder="Weight"
                value={bicepsExpanded}
                onChange={(e) => setBicepsExpanded(e.target.value)}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">cm</InputAdornment>
                  ),
                }}
              />
            </Stack>
          </Stack>

          <Stack width="45%">
            Forearms
            <TextField
              type="number"
              placeholder="Weight"
              value={forearms}
              onChange={(e) => setForearms(e.target.value)}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">cm</InputAdornment>
                ),
              }}
            />
          </Stack>

          <Stack width="45%">
            Thighs
            <TextField
              type="number"
              placeholder="Weight"
              value={thighs}
              onChange={(e) => setThighs(e.target.value)}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">cm</InputAdornment>
                ),
              }}
            />
          </Stack>

          <Stack width="45%">
            Calfs
            <TextField
              type="number"
              placeholder="Weight"
              value={calfs}
              onChange={(e) => setCalfs(e.target.value)}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">cm</InputAdornment>
                ),
              }}
            />
          </Stack>

          <Stack width="45%">
            Wrists
            <TextField
              type="number"
              placeholder="Weight"
              value={wrist}
              onChange={(e) => setWrist(e.target.value)}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">cm</InputAdornment>
                ),
              }}
            />
          </Stack>

          <StyledButtonSecondary type="submit">Submit</StyledButtonSecondary>
        </FormContainer>
      </Box>
    </Stack>
  )
}

export default MeasurementsScreen
