import React, { useEffect, useState } from "react"
import {
  Box,
  Stack,
  TextField,
  Typography,
  Select,
  MenuItem,
} from "@mui/material"
import {
  FormContainer,
  StyledButtonSecondary,
} from "../muiCustom/customComponents"
import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import { updateUser } from "../actions/userActions"

const ProfileScreen = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const [phone, setPhone] = useState(0)
  const [address, setAddress] = useState("")
  const [gender, setGender] = useState("")
  const [age, setAge] = useState(0)
  const [image, setImage] = useState("")

  const userLogin = useSelector((state) => state.userLogin)
  const { userInfo } = userLogin

  const submitHandler = (e) => {
    e.preventDefault()
    dispatch(updateUser({ name, email, phone, address, gender, age }))
    navigate("/")
  }

  useEffect(() => {
    window.scroll(0, 0)
    if (userInfo) {
      userInfo.name ? setName(userInfo.name) : ""
      userInfo.email ? setEmail(userInfo.email) : ""
      userInfo.phone ? setPhone(userInfo.phone) : ""
      userInfo.address ? setAddress(userInfo.address) : ""
      userInfo.gender ? setGender(userInfo.gender) : ""
      userInfo.age ? setAge(userInfo.age) : ""
    } else {
      navigate("/loginuser")
    }
  }, [userInfo])
  return (
    <Stack sx={{ alignItems: "center" }}>
      <Box
        sx={{
          width: { xs: "90vw", sm: "70vw", md: "40vw" },
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
        }}
      >
        <FormContainer
          component="form"
          onSubmit={submitHandler}
          sx={{ marginBottom: "5rem", marginTop: "1rem" }}
        >
          <Typography variant="h4" component="h2">
            Profile
          </Typography>

          <Stack width="100%">
            Name
            <TextField
              required
              type="text"
              placeholder="Full name"
              value={name}
              fullWidth
              onChange={(e) => setName(e.target.value)}
            />
          </Stack>
          <Stack width="100%">
            Email
            <TextField
              required
              type="text"
              placeholder="Email"
              fullWidth
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Stack>
          <Stack width="100%">
            Phone No
            <TextField
              required
              type="number"
              placeholder="Enter a valid phone number"
              value={phone ? phone : ""}
              fullWidth
              InputProps={{
                startAdornment: "+91-",
              }}
              onChange={(e) => setPhone(e.target.value)}
            />
          </Stack>
          <Stack width="100%">
            Address
            <TextField
              required
              placeholder="Address"
              value={address}
              multiline
              fullWidth
              onChange={(e) => setAddress(e.target.value)}
            />
          </Stack>
          <Stack width="100%">
            Gender
            <Select
              value={gender}
              onChange={(e) => setGender(e.target.value)}
              required
            >
              <MenuItem type="text" value="male">
                Male
              </MenuItem>
              <MenuItem type="text" value="female">
                Female
              </MenuItem>
            </Select>
          </Stack>

          <Stack width="100%">
            Age
            <TextField
              type="number"
              placeholder="Age"
              inputProps={{ maxLength: 3, min: 10, max: 100 }}
              value={age}
              onChange={(e) => setAge(e.target.value)}
            />
          </Stack>

          {/* <Stack width="100%">
            Image(Upload your Passport Size Photo)
            <Input required type="file" onChange={uploadFileHandler} />
            <Input
              onChange={(e) => setImage(e.target.value)}
              value={image}
              type="text"
              placeholder="Image url"
              inputProps={{ accept: `.jpg, .jpeg, .png` }}
              sx={{
                border: "1px solid gray",
                borderRadius: "5px",
                padding: "0.6rem",
              }}
            />
          </Stack> */}

          <StyledButtonSecondary type="submit">Submit</StyledButtonSecondary>
        </FormContainer>
      </Box>
    </Stack>
  )
}

export default ProfileScreen
