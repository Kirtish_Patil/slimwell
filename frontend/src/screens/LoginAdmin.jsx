import { Box, Link, Stack, TextField, Typography } from "@mui/material"
import {
  Link as RouterLink,
  useNavigate,
  useSearchParams,
} from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { useState, useEffect } from "react"
import {
  FormContainer,
  StyledButtonSecondary,
} from "../muiCustom/customComponents.jsx"
import Message from "../components/Message.jsx"
import AuthNav from "../components/AuthNav.jsx"
import { loginAdmin } from "../actions/adminActions.js"

const LoginAdmin = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const adminLogin = useSelector((state) => state.adminLogin)
  const { loading, error, adminInfo } = adminLogin

  const [searchParams] = useSearchParams()
  let redirect = searchParams.get("redirect") || "/"

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [verificationKey, setVerificationKey] = useState("")

  useEffect(() => {
    window.scrollTo(0, 0)
    if (adminInfo) {
      navigate(redirect)
    }
  }, [adminInfo, redirect])

  const submitHandler = (e) => {
    e.preventDefault()
    dispatch(loginAdmin(email, password, verificationKey))
  }

  return (
    <Stack
      direction="row"
      width="100%"
      height="100%"
      sx={{
        justifyContent: "center",
        alignItems: "center",
        display: "flex",
      }}
    >
      <Box
        sx={{
          width: { xs: "90vw", sm: "70vw", md: "40vw" },
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
          flexDirection: "column",
        }}
      >
        <FormContainer component="form" onSubmit={submitHandler}>
          <AuthNav user="/loginuser" admin="/loginadmin" />
          {error && <Message type="error">{error}</Message>}
          {adminInfo && (
            <Message type="info">Registered Successfully. Please Login</Message>
          )}

          {/* Email */}
          <Typography variant="h4" component="h2">
            Admin Login
          </Typography>
          <Stack width="100%">
            Email
            <TextField
              placeholder="Email"
              fullWidth
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Stack>

          {/* Password */}
          <Stack width="100%">
            Password
            <TextField
              placeholder="Password"
              type="password"
              fullWidth
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </Stack>

          <Stack width="100%">
            Verification Code
            <TextField
              placeholder="Enter admin verification Key"
              type="password"
              fullWidth
              value={verificationKey}
              onChange={(e) => setVerificationKey(e.target.value)}
            />
          </Stack>

          {/* Login and Register */}
          <Stack
            direction="column"
            spacing={2}
            sx={{ justifyContent: "center", alignItems: "flex-start" }}
          >
            <StyledButtonSecondary type="submit">Login</StyledButtonSecondary>{" "}
            <Typography variant="p">
              Already a Member?{" "}
              <Link
                component={RouterLink}
                to="/registeruser"
                sx={{ color: "black", fontWeight: "bold" }}
              >
                Click here to Register
              </Link>
            </Typography>
          </Stack>
        </FormContainer>
      </Box>
    </Stack>
  )
}

export default LoginAdmin
