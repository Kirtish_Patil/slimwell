import {
  Box,
  Checkbox,
  FormControlLabel,
  Grid,
  Link,
  Stack,
  TextField,
  Typography,
} from "@mui/material"
import {
  Link as RouterLink,
  useNavigate,
  useSearchParams,
} from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { useState, useEffect } from "react"
import { loginUser, logout } from "../actions/userActions.js"
import {
  FormContainer,
  StyledButtonSecondary,
} from "../muiCustom/customComponents.jsx"
import Message from "../components/Message.jsx"
import theme from "../theme.js"
import AuthNav from "../components/AuthNav.jsx"

const LoginUserScreen = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const userLogin = useSelector((state) => state.userLogin)
  const { loading, error, userInfo } = userLogin

  const [searchParams] = useSearchParams()
  let redirect = searchParams.get("redirect") || "/"

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  useEffect(() => {
    window.scrollTo(0, 0)
    if (userInfo) {
      navigate(redirect)
    }
  }, [userInfo, redirect])

  const submitHandler = (e) => {
    e.preventDefault()
    dispatch(loginUser(email, password))
  }

  return (
    <Stack
      direction="row"
      width="100%"
      height="100%"
      sx={{
        justifyContent: "center",
        alignItems: "center",
        display: "flex",
      }}
    >
      <Box
        sx={{
          width: { xs: "90vw", sm: "70vw", md: "40vw" },
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
          flexDirection: "column",
        }}
      >
        <FormContainer component="form" onSubmit={submitHandler}>
          <AuthNav user="/loginuser" admin="/loginadmin" />
          {error && <Message type="error">{error}</Message>}
          {userInfo && (
            <Message type="info">Registered Successfully. Please Login</Message>
          )}

          {/* Email */}
          <Typography variant="h4" component="h2">
            User Login
          </Typography>
          <Stack width="100%">
            Email
            <TextField
              placeholder="Email"
              fullWidth
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Stack>

          {/* Password */}
          <Stack width="100%">
            Password
            <TextField
              placeholder="Password"
              type="password"
              fullWidth
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </Stack>

          {/* Login and Register */}
          <Stack
            direction="column"
            spacing={2}
            sx={{ justifyContent: "center", alignItems: "flex-start" }}
          >
            <StyledButtonSecondary type="submit">Login</StyledButtonSecondary>{" "}
            <Typography variant="p">
              Already a Member?{" "}
              <Link
                component={RouterLink}
                to="/registeruser"
                sx={{ color: "black", fontWeight: "bold" }}
              >
                Click here to Register
              </Link>
            </Typography>
          </Stack>
        </FormContainer>
      </Box>
    </Stack>
  )
}

export default LoginUserScreen
