import { Grid, Box, Typography, Button } from "@mui/material"
import React, { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Link as RouterLink, useNavigate } from "react-router-dom"
import {
  FormContainer,
  StyledButtonSecondary,
} from "../muiCustom/customComponents"
import { makePayment } from "../actions/membershipActions"
import Message from "../components/Message"
import Loader from "../components/Loader"

const CheckoutScreen = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const membershipCreate = useSelector((state) => state.membershipCreate)
  const {
    loading: membershipLoading,
    error: membershipError,
    membership,
  } = membershipCreate

  const userUpdate = useSelector((state) => state.userUpdate)
  const { userInfo, success, error, loading } = userUpdate

  const membershipPayment = useSelector((state) => state.membershipPayment)
  const { payment } = membershipPayment

  const paymentHandler = async (req, res) => {
    dispatch(makePayment({ membership, userInfo }))
  }

  useEffect(() => {
    window.scroll(0, 0)
    if (payment.url) {
      window.location.href = payment.url
    }
  }, [payment])

  return (
    <Grid container justifyContent="center">
      <Grid item xs={12} md={6} mb={5}>
        <Box
          sx={{ width: "100%", padding: "5%" }}
          justifyContent="space-around"
        >
          {(loading || membershipLoading) && <Loader />}
          {error && <Message type="error">{error}</Message>}
          {userInfo && (
            <FormContainer
              sx={{
                margin: "0rem 0rem",
                marginTop: "2rem",
              }}
            >
              {/* Membership Details */}
              <Box
                display={{ xs: "grid", sm: "flex" }}
                justifyContent={{ xs: "start", sm: "space-between" }}
                alignItems="center"
              >
                <Box sx={{ marginBottom: { xs: "1rem", sm: "0" } }}>
                  <Typography variant="h4" component="h2">
                    Plan Details
                  </Typography>
                  <Typography fontWeight="300">
                    Verify your Selected Plan
                  </Typography>
                </Box>

                <Box>
                  <Typography variant="body" fontSize={20}>
                    <strong>Type: </strong> {membership.type}
                    <br />
                    <strong>Duration: </strong> {membership.duration}
                    <br />
                    <strong>Price: </strong> {membership.price}
                  </Typography>
                </Box>
              </Box>

              {/* User Details */}
              <Box
                display={{ xs: "grid", sm: "flex" }}
                justifyContent={{ xs: "start", sm: "space-between" }}
                alignItems="center"
              >
                <Box sx={{ marginBottom: { xs: "1rem", sm: "0" } }}>
                  <Typography variant="h4" component="h2">
                    User Details
                  </Typography>
                  <Typography fontWeight="300">Verify your Detials</Typography>
                </Box>

                <Box>
                  <Typography variant="body" fontSize={20}>
                    <strong>Email: </strong> {userInfo.email}
                    <br />
                    <strong>Name: </strong> {userInfo.name}
                    <br />
                    <strong>Address: </strong> {userInfo.address}
                    <br />
                    <strong>Age: </strong> {userInfo.age}
                    <br />
                    <strong>Gender: </strong> {userInfo.gender}
                    <br />
                    <strong>Contact No: </strong> {userInfo.phone}
                  </Typography>
                </Box>
              </Box>

              {/* Checkout  */}
              <Box justifyContent="center" display="flex" paddingY="2rem">
                <StyledButtonSecondary onClick={paymentHandler}>
                  Proceed To Checkout
                </StyledButtonSecondary>
              </Box>
            </FormContainer>
          )}
        </Box>
      </Grid>
    </Grid>
  )
}

export default CheckoutScreen
