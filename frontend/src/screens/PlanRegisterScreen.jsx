import React, { useEffect, useState } from "react"
import {
  Box,
  Stack,
  TextField,
  Typography,
  Select,
  MenuItem,
  Input,
  FormHelperText,
} from "@mui/material"
import {
  FormContainer,
  StyledButtonSecondary,
} from "../muiCustom/customComponents"
import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import { updateUser } from "../actions/userActions"

const PlanRegisterScreen = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const [phone, setPhone] = useState(0)
  const [address, setAddress] = useState("")
  const [gender, setGender] = useState("")
  const [age, setAge] = useState(0)
  const [image, setImage] = useState("")

  const userLogin = useSelector((state) => state.userLogin)
  const { userInfo } = userLogin

  const userUpdate = useSelector((state) => state.userUpdate)
  const { userInfo: updatedUserInfo, success, loading } = userUpdate

  const membershipCreate = useSelector((state) => state.membershipCreate)
  const { membership } = membershipCreate

  // const handleFileChange = (e) => {
  //   const file = e.target.file[0]
  //   const maxSizeInBytes = 500 * 1024

  //   if (file) {
  //     if (!["image/jpeg", "image/jpg", "image/png"].includes(file.type)) {
  //       alert("Please upload an image in JPG, JPEG, or PNG format.")
  //       return
  //     }

  //     if (file.size > maxSizeInBytes) {
  //       alert(
  //         "File size exceeds the limit of 500kb. Please select a smaller file"
  //       )
  //       return
  //     }
  //   }
  // }

  // const uploadFileHandler = async (e) => {
  //   handleFileChange()
  //   const file = e.target.files[0]
  //   const formData = new FormData()
  //   formData.append("image", file)

  //   try {
  //     const config = {
  //       headers: {
  //         "Content-Type": "multipart/form-data",
  //       },
  //     }

  //     const { data } = await axios.post(`/api/uploads`, formData, config)
  //     setImage(data)
  //   } catch (err) {
  //     console.error(err)
  //   }
  // }

  const submitHandler = (e) => {
    e.preventDefault()
    dispatch(updateUser({ name, email, phone, address, gender, age }))
    navigate("/checkout")
  }

  useEffect(() => {
    window.scroll(0, 0)
    if (userInfo) {
      if (!membership.price) {
        navigate("/membership")
      } else {
        userInfo.name ? setName(userInfo.name) : ""
        userInfo.email ? setEmail(userInfo.email) : ""
        userInfo.phone ? setPhone(userInfo.phone) : ""
        userInfo.address ? setAddress(userInfo.address) : ""
        userInfo.gender ? setGender(userInfo.gender) : ""
        userInfo.age ? setAge(userInfo.age) : ""
      }
    } else {
      navigate("/loginuser")
    }
  }, [userInfo, , membership])

  return (
    <Stack sx={{ alignItems: "center" }}>
      <Box sx={{ width: { xs: "90vw", sm: "70vw", md: "40vw" } }}>
        <FormContainer
          sx={{
            margin: "0rem 0rem",
            marginTop: "5rem",
          }}
        >
          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
          >
            <Box>
              <Typography variant="h4" component="h2">
                Plan Details
              </Typography>
              <Typography fontWeight="300">
                Verify your Selected Plan
              </Typography>
            </Box>

            <Box>
              <Typography variant="body" fontSize={20}>
                <strong>Type: </strong> {membership.type}
                <br />
                <strong>Duration: </strong> {membership.duration}
                <br />
                <strong>Price: </strong> {membership.price}
              </Typography>
            </Box>
          </Box>
        </FormContainer>
      </Box>
      <Box
        sx={{
          width: { xs: "90vw", sm: "70vw", md: "40vw" },
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
        }}
      >
        <FormContainer
          component="form"
          onSubmit={submitHandler}
          sx={{ marginBottom: "5rem", marginTop: "1rem" }}
        >
          <Typography variant="h4" component="h2">
            User Details
            <Typography fontWeight="300">
              Please Enter Additional Details
            </Typography>
          </Typography>

          <Stack width="100%">
            Name
            <TextField
              required
              type="text"
              placeholder="Full name"
              value={name}
              fullWidth
              onChange={(e) => setName(e.target.value)}
            />
          </Stack>
          <Stack width="100%">
            Email
            <TextField
              required
              type="text"
              placeholder="Email"
              fullWidth
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Stack>
          <Stack width="100%">
            Phone No
            <TextField
              required
              type="number"
              placeholder="Enter a valid phone number"
              value={phone ? phone : ""}
              fullWidth
              InputProps={{
                startAdornment: "+91-",
              }}
              onChange={(e) => setPhone(e.target.value)}
            />
          </Stack>
          <Stack width="100%">
            Address
            <TextField
              required
              placeholder="Address"
              value={address}
              multiline
              fullWidth
              onChange={(e) => setAddress(e.target.value)}
            />
          </Stack>
          <Stack width="100%">
            Gender
            <Select
              value={gender}
              onChange={(e) => setGender(e.target.value)}
              required
            >
              <MenuItem type="text" value="male">
                Male
              </MenuItem>
              <MenuItem type="text" value="female">
                Female
              </MenuItem>
            </Select>
          </Stack>

          <Stack width="100%">
            Age
            <TextField
              type="number"
              placeholder="Age"
              inputProps={{ maxLength: 3, min: 14, max: 100 }}
              value={age ? age : ""}
              onChange={(e) => setAge(e.target.value)}
            />
          </Stack>

          {/* <Stack width="100%">
            Image(Upload your Passport Size Photo)
            <Input required type="file" onChange={uploadFileHandler} />
            <Input
              onChange={(e) => setImage(e.target.value)}
              value={image}
              type="text"
              placeholder="Image url"
              inputProps={{ accept: `.jpg, .jpeg, .png` }}
              sx={{
                border: "1px solid gray",
                borderRadius: "5px",
                padding: "0.6rem",
              }}
            />
          </Stack> */}

          <StyledButtonSecondary type="submit">Submit</StyledButtonSecondary>
        </FormContainer>
      </Box>
    </Stack>
  )
}

export default PlanRegisterScreen
