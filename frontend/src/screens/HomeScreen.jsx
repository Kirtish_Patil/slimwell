import React, { useEffect } from "react"
import { Typography, Container, Box, List } from "@mui/material"
import theme from "../theme"
import { StyledButtonSecondary } from "../muiCustom/customComponents"
import { useNavigate } from "react-router-dom"
import { main } from "../contents/home"
import { useSelector } from "react-redux"

const HomeScreen = () => {
  const navigate = useNavigate()

  const userLogin = useSelector((state) => state.userLogin)
  const { userInfo } = userLogin

  const adminLogin = useSelector((state) => state.adminLogin)
  const { adminInfo } = adminLogin

  const getTrialHandler = () => {
    if (userInfo) {
      navigate("/loginuser?redirect=/trial")
    } else if (adminInfo) {
      navigate("/loginadmin?redirect=/trial")
    } else {
      navigate("/loginuser?redirect=/trial")
    }
  }

  useEffect(() => {
    window.scroll(0, 0)
  }, [])
  return (
    <>
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Box
          sx={{
            backgroundImage: `url(${main.gymImagePath})`,
            backgroundSize: "cover",
            backgroundPosition: "center",
            width: "100vw",
            color: "white",
            height: "90vh",
            display: "flex",
            flexDirection: "column",
            justifyContent: "flex-end",
          }}
        >
          <Box
            sx={{
              background: "rgba(0,0,0,0.6)",
              display: "flex",
              alignItems: { xs: "flex-end", sm: "center" },
              height: { xs: "100%", sm: "30%" },
              py: "2.5rem",
            }}
          >
            <Container>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: { xs: "column", sm: "row" },
                  justifyContent: "space-between",
                  width: { xs: "100%", sm: "30%" },
                }}
              >
                <Typography variant="h4">Try Out for Free</Typography>
                <StyledButtonSecondary
                  variant="contained"
                  onClick={getTrialHandler}
                >
                  Free Trial
                </StyledButtonSecondary>
              </Box>
              <Typography
                variant="p"
                display="block"
                sx={{ marginTop: "0.5rem", letterSpacing: "1px" }}
              >
                Get fit, feel great. Start your free trial with our{" "}
                <strong>SlimWell</strong> web app today!
              </Typography>
            </Container>
          </Box>
        </Box>
      </Box>
      <Box sx={{ backgroundColor: theme.palette.primary.light }}>
        <Container maxWidth="auto">
          <Box
            sx={{
              padding: "1rem",
              display: "flex",
              alignItems: "center",
              justifyContent: "space-around",
              bgcolor: "white",
              flexDirection: { xs: "column", sm: "row" },
            }}
          >
            <Box
              sx={{
                backgroundImage: `url(${main.image})`,
                backgroundSize: "contain",
                backgroundRepeat: "no-repeat",
                backgroundPosition: "center",
                textAlign: "start",
                height: { xs: "30vh", sm: "70vh" },
                width: { xs: "70vw", sm: "30vw" },
                flexFlow: "1",
                justifyContent: "center",
                alignItems: "center",
              }}
            />
            <Box
              sx={{
                display: "flex",
                width: "auto",
                flexDirection: "column",
                alignItems: { xs: "center", sm: "flex-start" },
              }}
            >
              <Typography variant="h6">{main.title}</Typography>
              <Typography variant="body">
                {main.subTitle} <strong>HM Gymnasium</strong>
                <br />
                <ul>
                  {main.description.map((i, idx) => (
                    <li key={idx}>{i}</li>
                  ))}
                </ul>
                <br />
              </Typography>
              <StyledButtonSecondary variant="contained">
                Find out more
              </StyledButtonSecondary>
            </Box>
            <Box></Box>
          </Box>
        </Container>
      </Box>
    </>
  )
}

export default HomeScreen
