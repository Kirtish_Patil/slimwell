import {
  Box,
  Link,
  Stack,
  Typography,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  FormLabel,
} from "@mui/material"

import {
  FormContainer,
  StyledButtonSecondary,
  StyledTextField,
} from "../muiCustom/customComponents.jsx"
import { useDispatch, useSelector } from "react-redux"
import {
  Link as RouterLink,
  useNavigate,
  useSearchParams,
} from "react-router-dom"
import { useEffect, useState } from "react"
import Message from "../components/Message.jsx"
import AuthNav from "../components/AuthNav.jsx"
import { registerAdmin } from "../actions/adminActions.js"

const AdminRegisterScreen = () => {
  const dispath = useDispatch()
  const navigate = useNavigate()

  const adminRegister = useSelector((state) => state.adminRegister)
  const { loading, error, adminInfo } = adminRegister
  const [searchParams] = useSearchParams()
  let redirect = searchParams.get("redirect") || "/logiadmin"

  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [confirmPassword, setConfirmPassword] = useState("")
  const [position, setPosition] = useState("")
  const [verificationKey, setVerificationKey] = useState("")
  const [message, setMessage] = useState("")

  const submitHandler = (e) => {
    e.preventDefault()
    window.scroll(0,0)
    if (password !== confirmPassword) {
      setMessage("Passwords did not match")
    } else {
      dispath(registerAdmin(name, email, password, position, verificationKey))
      navigate("/admininvite")
    }
  }

  useEffect(() => {
    if (adminInfo) {
      navigate(redirect)
    }
  }, [adminInfo, redirect])

  return (
    <Stack
      direction="row"
      sx={{
        justifyContent: "center",
        alignItems: "center",
        display: "flex",
      }}
    >
      <Box
        sx={{
            width: { xs: "90vw", sm: "70vw", md: "40vw" },
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
        }}
      >
        <FormContainer component="form" onSubmit={submitHandler}>
          <AuthNav user="/registeruser" admin="/registeradmin" />

          {error && <Message type="error">{error}</Message>}
          {message && <Message type="error">{message}</Message>}

          <Typography variant="h4" component="h2">
            Admin Register
          </Typography>

          <Stack width="100%">
            <FormLabel required sx={{ color: "black" }}>
              Name
            </FormLabel>
            <StyledTextField
              placeholder="Name"
              required
              fullWidth
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </Stack>

          <Stack width="100%">
            <FormLabel required sx={{ color: "black" }}>
              Email
            </FormLabel>
            <StyledTextField
              placeholder="Email"
              required
              fullWidth
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Stack>

          <Stack width="100%">
            <FormLabel required sx={{ color: "black" }}>
              Password
            </FormLabel>
            <StyledTextField
              placeholder="Password"
              type="password"
              required
              fullWidth
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </Stack>

          <Stack width="100%">
            <FormLabel required sx={{ color: "black" }}>
              Confirm Password
            </FormLabel>
            <StyledTextField
              placeholder="Re-enter you password"
              type="password"
              required
              fullWidth
              value={confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
            />
          </Stack>

          <Stack direction="column" sx={{ width: "100%" }}>
            <FormControl fullWidth focused={false}>
              <FormLabel id="position-select" required sx={{ color: "black" }}>
                Position
              </FormLabel>
              <Select
                labelId="position-select"
                value={position}
                onChange={(e) => setPosition(e.target.value)}
              >
                <MenuItem type="text" value="manager">
                  Manager
                </MenuItem>
                <MenuItem type="text" value="trainer">
                  Trainer
                </MenuItem>
                <MenuItem type="text" value="reception">
                  Reception
                </MenuItem>
                <MenuItem type="text" value="owner">
                  Owner
                </MenuItem>
              </Select>
            </FormControl>
          </Stack>

          {position === "owner" && (
            <Stack width="100%">
              <FormLabel required sx={{ color: "black" }}>
                Verification Key
              </FormLabel>
              <StyledTextField
                placeholder="Enter your verification key"
                type="password"
                required
                fullWidth
                value={verificationKey}
                onChange={(e) => setVerificationKey(e.target.value)}
              />
            </Stack>
          )}

          <Stack
            direction="column"
            spacing={2}
            sx={{ justifyContent: "center", alignItems: "flex-start" }}
          >
            <StyledButtonSecondary type="submit">
              Register
            </StyledButtonSecondary>{" "}
            <Typography variant="p">
              Already a Member?{" "}
              <Link
                component={RouterLink}
                to="/loginuser"
                sx={{ color: "black", fontWeight: "bold" }}
              >
                Click here to Login
              </Link>
            </Typography>
          </Stack>
        </FormContainer>
      </Box>
    </Stack>
  )
}

export default AdminRegisterScreen
