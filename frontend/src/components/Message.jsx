import { Alert } from "@mui/material"
import React from "react"

const Message = ({ type = "info", children }) => {
  return (
    <Alert severity={type} variant="filled" sx={{ width: "100%" }}>
      {children}
    </Alert>
  )
}

export default Message
