import {
  AppBar,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
  Stack,
  ButtonBase,
  IconButton,
} from "@mui/material"
import AccountCircleIcon from "@mui/icons-material/AccountCircle"
import React, { useState } from "react"
import theme from "../theme"
import { useDispatch, useSelector } from "react-redux"
import { Link as RouterLink, useNavigate } from "react-router-dom"
import { StyledButton } from "../muiCustom/customComponents.jsx"
import { logout } from "../actions/userActions.js"
import { ConstructionOutlined } from "@mui/icons-material"

const Header = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const state = useSelector((state) => state)
  // console.log(state)

  const getTrialHandler = () => {
    if (userInfo) {
      navigate("/loginuser?redirect=/trial")
    } else if (adminInfo) {
      navigate("/loginadmin?redirect=/trial")
    } else {
      navigate("/loginuser?redirect=/trial")
    }
  }

  const getMembershipHandler = () => {
    if (userInfo) {
      navigate("/loginuser?redirect=/membership")
    } else if (adminInfo) {
      navigate("/loginadmin?redirect=/membership")
    } else {
      navigate("/loginuser?redirect=/membership")
    }
  }

  const logoutHandler = () => {
    dispatch(logout())
    setAnchorEl(null)
    navigate("/loginuser")
    window.location.reload()
  }

  const updateProfileHandler = () => {
    navigate('/profile')
    setAnchorEl(null)
  }

  const getMeasurementHandler = () => {
    navigate('/measurements')
    setAnchorEl(null)
  }

  const userLogin = useSelector((state) => state.userLogin)
  const { userInfo } = userLogin
  const adminLogin = useSelector((state) => state.adminLogin)
  const { adminInfo } = adminLogin

  // mui logic
  const [anchorEl, setAnchorEl] = React.useState(null)
  const open = Boolean(anchorEl)
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }
  return (
    <>
      <AppBar
        position="sticky"
        sx={{
          bgcolor: "white",
          alignItems: "space-between",
          justifyContent: "center",
          borderBottom: 1,
          borderColor: theme.palette.primary.dark,
        }}
      >
        <Toolbar
          disableGutters
          sx={{
            justifyContent: "space-between",
            paddingLeft: { xs: "0.2rem", sm: "3rem" },
          }}
        >
          <Stack direction="row">
            {(userInfo || adminInfo) && (
              <>
                <IconButton
                  id="demo-positioned-button"
                  aria-controls={open ? "demo-positioned-menu" : undefined}
                  aria-haspopup="true"
                  aria-expanded={open ? "true" : undefined}
                  onClick={handleClick}
                  size="large"
                  color="secondary"
                  sx={{ paddingLeft: "1rem", margin: "0" }}
                >
                  <AccountCircleIcon />
                </IconButton>
                <Menu
                  id="demo-positioned-menu"
                  anchorEl={anchorEl}
                  open={open}
                  onClose={handleClose}
                  anchorOrigin={{
                    vertical: "top",
                    horizontal: "left",
                  }}
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "left",
                  }}
                >
                  <MenuItem onClick={updateProfileHandler}>Profile</MenuItem>
                  <MenuItem onClick={getMeasurementHandler}>Measurements</MenuItem>
                  <MenuItem onClick={logoutHandler}>Logout</MenuItem>
                </Menu>
              </>
            )}

            <ButtonBase as={RouterLink} to="/" sx={{ display: "flex" }}>
              <Typography
                variant="h1"
                sx={{
                  fontWeight: "light",
                  fontSize: "2rem",
                  color: theme.palette.primary.contrastText,
                  paddingLeft: "0.2rem",
                }}
              >
                SlimWell
              </Typography>
            </ButtonBase>
          </Stack>

          {/* Navigation Buttons */}
          <Stack direction="row" height="72px">
            <StyledButton
              sx={{
                bgcolor: theme.palette.primary.light,
                flex: 1,
                height: "100%",
                "&:hover": {
                  backgroundColor: theme.palette.primary.main,
                },
              }}
              onClick={getMembershipHandler}
            >
              <Typography
                sx={{ fontWeight: "bold", textTransform: "capitalize" }}
              >
                Join Now
              </Typography>
            </StyledButton>

            <StyledButton
              sx={{
                bgcolor: theme.palette.info.dark,
                color: "white",
                "&:hover": {
                  backgroundColor: theme.palette.info.main,
                },
              }}
              onClick={getTrialHandler}
            >
              <Typography
                sx={{ fontWeight: "bold", textTransform: "capitalize" }}
              >
                Free Trial
              </Typography>
            </StyledButton>
          </Stack>
        </Toolbar>
      </AppBar>
    </>
  )
}

export default Header
