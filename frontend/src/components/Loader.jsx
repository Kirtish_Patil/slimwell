import React from "react"
import { CircularProgress } from "@mui/material"

const Loader = () => {
  return <CircularProgress color="secondary" sx={{ mt: "1rem" }} />
}

export default Loader
