import React from "react"
import { Typography, Link, Breadcrumbs, Box } from "@mui/material"
import { Link as RouterLink } from "react-router-dom"

const AuthNav = ({ user, admin }) => {
  return (
    <Box
      display="flex"
      sx={{
        width: "100%",
        justifyContent: "flex",
      }}
    >
      <Breadcrumbs separator="|" sx={{ justifyContent: "space-between" }}>
        <Link
          as={RouterLink}
          color="inherit"
          to={user}
          sx={{ textDecoration: "none", "&:hover": { color: "black" } }}
        >
          User
        </Link>
        <Link
          as={RouterLink}
          color="inherit"
          to={admin}
          sx={{ textDecoration: "none", "&:hover": { color: "black" } }}
        >
          Admin
        </Link>
      </Breadcrumbs>
    </Box>
  )
}

export default AuthNav
