import React from "react"
import {
  Badge,
  Box,
  Button,
  ButtonBase,
  Container,
  IconButton,
  Stack,
  Typography,
} from "@mui/material"
import {Instagram, Facebook, WhatsApp} from '@mui/icons-material'
import theme from "../theme"

const Footer = () => {
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "flex-start",
        paddingY: 3,
        bgcolor: "black",
        alignItems: "center",
        direction: "column",
      }}
    >
      <Container maxWidth="xl">
        <Typography
          display="block"
          sx={{
            color: "white",
            fontWeight: "bold",
            fontSize: "2rem",
            borderBottom: 1,
          }}
        >
          SLIM WELL
        </Typography>

        <Stack
          direction="row"
          justifyContent="space-between"
          paddingY="1rem"
          mt="1rem"
        >
          <Stack color="white" direction='row' spacing='0.4rem'>
            <IconButton color="inherit">
              <Instagram />
            </IconButton>
            <IconButton color="inherit">
              <Facebook />
            </IconButton>
            <IconButton color="inherit">
              <WhatsApp />
            </IconButton>
          </Stack>
          <Typography sx={{ color: "white" }} width="30%">
            Copyright {new Date().getFullYear()}. Slim Well All Rights Reserved.
          </Typography>
        </Stack>
      </Container>
    </Box>
  )
}

export default Footer
