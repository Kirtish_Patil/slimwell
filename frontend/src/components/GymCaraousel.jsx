import { Box, Stack, Button, Typography, ButtonBase, Link } from "@mui/material"
import React, { useEffect, useState } from "react"
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos"
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos"
import LocationOnIcon from "@mui/icons-material/LocationOn"
import theme from "../theme"
import { StyledButtonSecondary } from "../muiCustom/customComponents"
import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import { createTrial } from "../actions/trialActions"

const GymCaraousel = ({ gym, idx }) => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const userLogin = useSelector((state) => state.userLogin)
  const { userInfo } = userLogin

  const trialCreate = useSelector((state) => state.trialCreate)
  const { success, trial } = trialCreate

  const [imageIndex, setImageIndex] = useState(0)

  const prevImage = () => {
    setImageIndex((currIndex) => Math.max(currIndex - 1, 0))
  }
  const nextImage = () => {
    setImageIndex((currIndex) => Math.min(currIndex + 1, gym.imgs.length - 1))
  }

  const getTrialHandler = () => {
    dispatch(createTrial(userInfo._id, gym._id))
  }

  useEffect(() => {
    if (success) {
      navigate(`/trialsuccess/${trial._id}`)
    }
  })

  return (
    <div>
      {trial && (
        <Stack key={idx}>
          <Box
            sx={{
              backgroundImage: `url(${gym.imgs[imageIndex].imgPath})`,
              backgroundSize: "cover",
              backgroundPosition: "center",
              width: "99vw",
              height: "80vh",
              marginBottom: "10px",
            }}
          >
            <Box
              sx={{
                background: "rgba(0,0,0,0.6)",
                width: "100%",
                height: "100%",
              }}
            >
              <Stack
                direction="row"
                justifyContent="space-between"
                alignItems="center"
                color="white"
              >
                <Button
                  onClick={prevImage}
                  sx={{ height: "80vh", width: "5rem" }}
                >
                  <ArrowBackIosIcon />
                </Button>
                <Box
                  sx={{
                    width: "100%",
                  }}
                >
                  <Stack sx={{ width: { xs: "100%", sm: "50%", md: "30%" } }}>
                    <Typography variant="h2" component="h3">
                      {gym.name}
                    </Typography>
                    <Typography>{gym.address}</Typography>
                    <Button
                      onClick={() => window.open(gym.location)}
                      variant="outlined"
                      color="secondary"
                      sx={{
                        width: "50%",
                        color: "white",
                        borderColor: theme.palette.primary.main,
                        borderWidth: "2px",
                        "&:hover": {
                          backgroundColor: theme.palette.primary.main,
                          borderColor: theme.palette.secondary.main,
                          paddingY: "0.5rem",
                          marginY: "0.5rem",
                          color: "black",
                        },
                        paddingY: "0.5rem",
                        marginY: "0.5rem",
                      }}
                    >
                      <LocationOnIcon />
                      Locate Us
                    </Button>
                    <StyledButtonSecondary
                      onClick={getTrialHandler}
                      sx={{ width: "50%", paddingY: "0.7rem" }}
                    >
                      Get Free Trial Now
                    </StyledButtonSecondary>
                  </Stack>
                </Box>
                <Button
                  onClick={nextImage}
                  sx={{ height: "80vh", width: "5rem" }}
                >
                  <ArrowForwardIosIcon />
                </Button>
              </Stack>
            </Box>
          </Box>
        </Stack>
      )}
    </div>
  )
}

export default GymCaraousel
