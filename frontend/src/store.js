import { applyMiddleware, createStore, combineReducers } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"
import thunk from "redux-thunk"
import {
  userUpdateReducer,
  userLoginReducer,
  userRegisterReducer,
} from "./reducer/userReducer.js"
import {
  adminLoginReducer,
  adminRegisterReducer,
} from "./reducer/adminReducer.js"
import { gymsListReducer } from "./reducer/gymsReducer.js"
import {
  trialCreateReducer,
  trialDetailsReducer,
} from "./reducer/trialReducer.js"
import {
  membershipCreateReducer,
  membershipPaymentReducer,
} from "./reducer/membershipReducer.js"
import { measurementCreateReducer } from "./reducer/measurementReducer.js"

const reducer = combineReducers({
  userLogin: userLoginReducer,
  userRegister: userRegisterReducer,
  userUpdate: userUpdateReducer,
  adminLogin: adminLoginReducer,
  adminRegister: adminRegisterReducer,
  gymsList: gymsListReducer,
  trialCreate: trialCreateReducer,
  trialDetails: trialDetailsReducer,
  membershipCreate: membershipCreateReducer,
  membershipPayment: membershipPaymentReducer,
  measurementCreate: measurementCreateReducer,
})

const userInfoFormStorage = localStorage.getItem("userInfo")
  ? JSON.parse(localStorage.getItem("userInfo"))
  : null

const adminInfoFromStorage = localStorage.getItem("adminInfo")
  ? JSON.parse(localStorage.getItem("adminInfo"))
  : null

const membershipInfoFromStorage = localStorage.getItem("membershipInfo")
  ? JSON.parse(localStorage.getItem("membershipInfo"))
  : null

const enrolledUserInfoFromStorage = localStorage.getItem("enrolledUserInfo")
  ? JSON.parse(localStorage.getItem("enrolledUserInfo"))
  : null

const measurementsFromStorage = localStorage.getItem("measurements")
  ? JSON.parse(localStorage.getItem("measurements"))
  : null

const initialState = {
  userLogin: {
    userInfo: userInfoFormStorage,
  },
  adminLogin: {
    adminInfo: adminInfoFromStorage,
  },
  membershipCreate: {
    membership: membershipInfoFromStorage,
  },
  userUpdate: {
    userInfo: enrolledUserInfoFromStorage,
  },
  measurementCreate: {
    measurement: measurementsFromStorage,
  },
}

const middlewares = [thunk]

const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middlewares))
)

export default store
