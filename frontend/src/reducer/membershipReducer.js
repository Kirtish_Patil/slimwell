import {
  MEMBERSHIP_CREATE,
  MEMBERSHIP_RESET,
  PAYMENT_FAIL,
  PAYMENT_REQUEST,
  PAYMENT_RESET,
  PAYMENT_SUCCESS,
} from "../constants/membershipConstants.js"

const membershipCreateReducer = (
  state = { success: false, membership: {} },
  action
) => {
  switch (action.type) {
    case MEMBERSHIP_CREATE:
      return { success: true, membership: action.payload }
    case MEMBERSHIP_RESET:
      return { membership: {}, success: false }
    default:
      return state
  }
}

const membershipPaymentReducer = (state = { payment: {} }, action) => {
  switch (action.type) {
    case PAYMENT_REQUEST:
      return { loading: true, ...state }
    case PAYMENT_SUCCESS:
      return { loading: false, payment: action.payload }
    case PAYMENT_FAIL:
      return { loading: false, error: action.payload }
    case PAYMENT_RESET:
      return { payment: {} }
    default:
      return state
  }
}

export { membershipCreateReducer, membershipPaymentReducer }
