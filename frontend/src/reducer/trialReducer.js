import {
  TRIAL_CREATE_FAIL,
  TRIAL_CREATE_REQUEST,
  TRIAL_CREATE_RESET,
  TRIAL_CREATE_SUCCESS,
  TRIAL_DETAILS_FAIL,
  TRIAL_DETAILS_REQUEST,
  TRIAL_DETAILS_RESET,
  TRIAL_DETAILS_SUCCESS,
} from "../constants/trialConstants"

const trialCreateReducer = (state = { trial: {} }, action) => {
  switch (action.type) {
    case TRIAL_CREATE_REQUEST:
      return { loading: true, ...state }
    case TRIAL_CREATE_SUCCESS:
      return { loading: false, success: true, trial: action.payload }
    case TRIAL_CREATE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case TRIAL_CREATE_RESET:
      return {}
    default:
      return state
  }
}

const trialDetailsReducer = (state = { trial: {} }, action) => {
  switch (action.type) {
    case TRIAL_DETAILS_REQUEST:
      return { loading: true, ...state }
    case TRIAL_DETAILS_SUCCESS:
      return { loading: false, trial: action.payload }
    case TRIAL_DETAILS_FAIL:
      return { loading: false, error: action.payload }
    case TRIAL_DETAILS_RESET:
      return { trial: {} }
    default:
      return state
  }
}

export { trialCreateReducer, trialDetailsReducer }
