import {
  MEASUREMENT_CREATE_FAIL,
  MEASUREMENT_CREATE_REQUEST,
  MEASUREMENT_CREATE_RESET,
  MEASUREMENT_CREATE_SUCCESS,
} from "../constants/measurementConstants"

const measurementCreateReducer = (state = {}, action) => {
  switch (action.type) {
    case MEASUREMENT_CREATE_REQUEST:
      return { loading: true }
    case MEASUREMENT_CREATE_SUCCESS:
      return { loading: false, measurement: action.payload }
    case MEASUREMENT_CREATE_FAIL:
      return { loading: false, error: action.payload }
    case MEASUREMENT_CREATE_RESET:
      return {}
    default:
      return state
  }
}

export { measurementCreateReducer }
