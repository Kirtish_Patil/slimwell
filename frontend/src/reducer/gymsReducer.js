import {
  GYM_LIST_SUCCESS,
  GYM_LIST_FAIL,
  GYM_LIST_REQUEST,
  GYM_LIST_RESET,
} from "../constants/gymConstants.js"

const gymsListReducer = (state = { loading: true, gyms: [] }, action) => {
  switch (action.type) {
    case GYM_LIST_REQUEST:
      return { loading: true, ...state }
    case GYM_LIST_SUCCESS:
      return { loading: false, gyms: action.payload }
    case GYM_LIST_FAIL:
      return { loading: false, error: action.payload }
    case GYM_LIST_RESET:
      return {}
    default:
      return state
  }
}

export { gymsListReducer }
