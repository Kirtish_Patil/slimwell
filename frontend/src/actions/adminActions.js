import {
  ADMIN_LOGIN_REQUEST,
  ADMIN_LOGIN_FAIL,
  ADMIN_LOGIN_SUCCESS,
  ADMIN_REGISTER_FAIL,
  ADMIN_REGISTER_REQUEST,
  ADMIN_REGISTER_SUCCESS,
} from "../constants/adminConstants"
import axios from "axios"

const loginAdmin = (email, password, verificationKey) => async (dispatch) => {
  try {
    dispatch({ type: ADMIN_LOGIN_REQUEST })

    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    }

    const { data } = await axios.post(
      "/api/admins/loginadmin",
      { email, password, verificationKey },
      config
    )

    dispatch({ type: ADMIN_LOGIN_SUCCESS, payload: data })
    localStorage.setItem("adminInfo", JSON.stringify(data))
  } catch (err) {
    dispatch({
      type: ADMIN_LOGIN_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    })
  }
}

const registerAdmin =
  (name, email, password, position, verificationKey) => async (dispatch) => {
    try {
      dispatch({ type: ADMIN_REGISTER_REQUEST })

      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      }

      const { data } = await axios.post(
        "/api/admins",
        { name, email, password, position, verificationKey },
        config
      )

      dispatch({ type: ADMIN_REGISTER_SUCCESS, payload: data })
    } catch (err) {
      dispatch({
        type: ADMIN_REGISTER_FAIL,
        payload:
          err.response && err.response.data.message
            ? err.response.data.message
            : err.message,
      })
    }
  }

export { loginAdmin, registerAdmin }
