import axios from "axios"
import {
  GYM_LIST_REQUEST,
  GYM_LIST_FAIL,
  GYM_LIST_SUCCESS,
} from "../constants/gymConstants"

const listGyms = () => async (dispatch, getState) => {
  try {
    dispatch({ type: GYM_LIST_REQUEST })

    const {
      userLogin: { userInfo },
      adminLogin: { adminInfo },
    } = getState()

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token || adminInfo.token}`,
      },
    }

    const { data } = await axios.get("/api/gyms", config)

    dispatch({ type: GYM_LIST_SUCCESS, payload: data })
  } catch (err) {
    dispatch({
      type: GYM_LIST_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    })
  }
}

export { listGyms }
