import {
  TRIAL_CREATE_FAIL,
  TRIAL_CREATE_REQUEST,
  TRIAL_CREATE_SUCCESS,
  TRIAL_DETAILS_FAIL,
  TRIAL_DETAILS_REQUEST,
  TRIAL_DETAILS_SUCCESS,
} from "../constants/trialConstants"
import axios from "axios"

const createTrial = (user, gym) => async (dispatch, getState) => {
  try {
    dispatch({ type: TRIAL_CREATE_REQUEST })

    const {
      userLogin: { userInfo },
      adminLogin: { adminInfo },
    } = getState()

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token || adminInfo.token}`,
        "Content-Type": "application/json",
      },
    }

    const { data } = await axios.post("/api/trial/", { user, gym }, config)

    dispatch({ type: TRIAL_CREATE_SUCCESS, payload: data })
  } catch (err) {
    dispatch({
      type: TRIAL_CREATE_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    })
  }
}

const getTrialDetails = (id) => async (dispatch, getState) => {
  try {
    dispatch({ type: TRIAL_DETAILS_REQUEST })

    const {
      userLogin: { userInfo },
    } = getState()

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    }

    const { data } = await axios.get(`/api/trial/${id}`, config)

    dispatch({ type: TRIAL_DETAILS_SUCCESS, payload: data })
  } catch (err) {
    dispatch({
      type: TRIAL_DETAILS_FAIL,
      error:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    })
  }
}

export { createTrial, getTrialDetails }
