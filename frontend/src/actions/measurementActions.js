import {
  MEASUREMENT_CREATE_FAIL,
  MEASUREMENT_CREATE_REQUEST,
  MEASUREMENT_CREATE_SUCCESS,
} from "../constants/measurementConstants.js"
import axios from "axios"

const createMeasurement =
  (userId, measurements) => async (dispatch, getState) => {
    try {
      dispatch({ type: MEASUREMENT_CREATE_REQUEST })
      
      const {
        userLogin: { userInfo },
      } = getState()
      
      const config = {
        headers: {
          Authorization: `Bearer ${userInfo.token}`,
          "Content-Type": "application/json",
        },
      }
      
      const { data } = await axios.post(
        "/api/measurements",
        { userId, measurements },
        config
        )

        console.log("i ran")

      dispatch({ type: MEASUREMENT_CREATE_SUCCESS, payload: data })
      localStorage.setItem("measurements", JSON.stringify(data))
    } catch (err) {
      dispatch({
        type: MEASUREMENT_CREATE_FAIL,
        error:
          err.response && err.response.data.message
            ? err.response.data.message
            : err.message,
      })
    }
  }

export { createMeasurement }
