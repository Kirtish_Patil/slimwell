import { ADMIN_LOGOUT, ADMIN_REGISTER_RESET } from "../constants/adminConstants"
import {
  USER_LOGIN_REQUEST,
  USER_LOGIN_FAIL,
  USER_LOGIN_SUCCESS,
  USER_LOGOUT,
  USER_REGISTER_FAIL,
  USER_REGISTER_REQUEST,
  USER_REGISTER_SUCCESS,
  USER_REGISTER_RESET,
  USER_UPDATE_FAIL,
  USER_UPDATE_REQUEST,
  USER_UPDATE_SUCCESS,
} from "../constants/userConstants"
import { GYM_LIST_RESET } from "../constants/gymConstants.js"
import {
  TRIAL_CREATE_RESET,
  TRIAL_DETAILS_RESET,
} from "../constants/trialConstants.js"
import axios from "axios"
import { MEMBERSHIP_RESET } from "../constants/membershipConstants.js"

const loginUser = (email, password, isAdmin, secretKey) => async (dispatch) => {
  try {
    dispatch({ type: USER_LOGIN_REQUEST })

    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    }

    const { data } = await axios.post(
      `/api/users/loginuser`,
      { email, password, isAdmin, secretKey },
      config
    )

    dispatch({ type: USER_LOGIN_SUCCESS, payload: data })
    localStorage.setItem("userInfo", JSON.stringify(data))
  } catch (err) {
    dispatch({
      type: USER_LOGIN_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    })
  }
}

const logout = () => async (dispatch) => {
  localStorage.removeItem("userInfo")
  localStorage.removeItem("adminInfo")
  localStorage.removeItem("membershipInfo")
  localStorage.removeItem("enrolledUserInfo")
  dispatch({ type: USER_LOGOUT })
  dispatch({ type: ADMIN_LOGOUT })
  dispatch({ type: ADMIN_REGISTER_RESET })
  dispatch({ type: USER_REGISTER_RESET })
  dispatch({ type: TRIAL_DETAILS_RESET })
  dispatch({ type: TRIAL_CREATE_RESET })
  dispatch({ type: GYM_LIST_RESET })
  dispatch({ type: MEMBERSHIP_RESET })
}

const registerUser = (name, email, password) => async (dispatch) => {
  try {
    dispatch({ type: USER_REGISTER_REQUEST })

    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    }

    const { data } = await axios.post(
      "/api/users",
      {
        name,
        email,
        password,
      },
      config
    )

    dispatch({ type: USER_REGISTER_SUCCESS, payload: data })
    dispatch({ type: USER_LOGIN_SUCCESS, payload: data })
    localStorage.setItem("userInfo", JSON.stringify(data))
  } catch (err) {
    dispatch({
      type: USER_REGISTER_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    })
  }
}

const updateUser = (user) => async (dispatch, getState) => {
  try {
    dispatch({ type: USER_UPDATE_REQUEST })

    const {
      userLogin: { userInfo },
    } = getState()

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
        "Content-Type": "application/json",
      },
    }

    const { data } = await axios.put("/api/users", user, config)

    dispatch({ type: USER_UPDATE_SUCCESS, payload: data })
    localStorage.setItem("enrolledUserInfo", JSON.stringify(data))
  } catch (err) {
    dispatch({
      type: USER_UPDATE_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    })
  }
}

export { loginUser, logout, registerUser, updateUser }
