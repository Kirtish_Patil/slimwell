import axios from "axios"
import {
  MEMBERSHIP_CREATE,
  PAYMENT_FAIL,
  PAYMENT_REQUEST,
  PAYMENT_SUCCESS,
} from "../constants/membershipConstants"
import { loadStripe } from "@stripe/stripe-js"

const createMembership = (membership) => async (dispatch) => {
  dispatch({ type: MEMBERSHIP_CREATE, payload: membership })

  localStorage.setItem("membershipInfo", JSON.stringify(membership))
}

const makePayment = (member) => async (dispatch) => {
  try {
    dispatch({ type: PAYMENT_REQUEST })

    const stripe = await loadStripe(
      "pk_test_51OPkQQSGUSEokzlg4hsx1le5xQN9axmEUZuUrTe9o5jJGc5rfjB2KEHkRwToLyQ9ewvv9KKsmCKseqNExFhBtCKm00eIc2eVxa"
    )

    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    }

    const { data } = await axios.post(
      "http://localhost:5002/create-checkout-session",
      { member },
      config
    )

    dispatch({ type: PAYMENT_SUCCESS, payload: data })
  } catch (err) {
    dispatch({
      type: PAYMENT_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    })
  }
}

export { createMembership, makePayment }
